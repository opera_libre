%------------------------------------------------------------------%
% Opéra Libre -- texte.ly                                          %
%                                                                  %
% (c) Lewis Trondheim & Valentin Villenave, 2008                   %
%                                                                  %
%------------------------------------------------------------------%

%% Depends on: text-functions.ly

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                        AFFAIRE ETRANGERE                         %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Prologue                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PrologueTenorTexte = \lyricmode {
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- té_!
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- té_!
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- téEH_!!!
  Un é -- tran -- ger __ _ dans u -- ne cu -- ri -- eu -- se
  ma -- chi -- ne vo -- lan -- te s'est __ _ é -- _ cra -- _ sé
  sur no -- tre châ -- teau…
  Aah_! __ _ Aah_! __ _ Aaah… __ _ _ Aah_! __ _ Aah_! __ _
  Me pu -- nir in -- ce -- ssa -- ment, Ma -- jes -- té_;
  j'ai dit_: \leftSyl « no -- _ tre châ -- teau » au lieu de_: \leftSyl « vo -- tre châ -- teau » __ _ _
  \dash C'est -- \dash à -- di -- re, Ma -- jes -- té_?
  Non, Ma -- jes -- té. Non, Ma -- jes -- té. Non, Ma -- jes -- té. Non, Ma -- jes -- té. Non, Ma -- jes -- té.
  Non  Ma -- jes -- t…
  C'est que per -- so -- nne ne le co -- nnaît, Ma -- jes -- té…
  Non_! Non non non non non non non non non non non non non non non non non, pen -- \dash sez -- vous_!
  Il est ho -- rrible à voir, ho -- rrible __ _ à voir_; il a PLEIN de sang sur le vi -- sage…
  Aah_! __ _ _ Aah_! __ _ _
  Aaa…__ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  Vous __ _ de -- vez pren -- dre la seu -- le dé -- ci -- si -- on qui s'im -- po -- se.
  Ce -- lle que vous choi -- si -- rez, et qui res -- te -- ra dans les li -- _ vres d'His -- toi -- re.
  Je __ _ ne __ _ sais pas_; __ _ _ _ _ _ _ _ _ seu -- _ le vo -- tre Ma -- jes -- té
  peut a -- voir u -- ne  telle i -- dée su -- blime_; moi je ne suis qu'un ver -- mi -- sseau.
  Su -- blime, ô __ _ Ma -- jes -- té_! SU_-_ BLIME.
  Hi_! Hi hi_! Hi hi hi_!
  Quand le Roi s'é -- loigne, il est tout pe -- tit_!
}

PrologueBarytonDeuxTexte = \lyricmode {
  Quoi, quoi, __ _ quoi_;
  qu'y \dash a -- \leftSyl t_-_il,
  qu'y \dash a -- \leftSyl t_-_il,
  qu'y \dash a -- \leftSyl t_-_il_?
  L'a -- ffaire est gra -- ve, très gra -- ve_;
  ex -- trê -- me -- ment gra -- _ _ ve.
  Un é -- tran -- ger, i -- ci_? Il faut a -- gir.
  Un é -- tran -- ger, \dash dis -- tu_?
  Ja -- mais je n'ai vu d'é -- tran -- ger par chez nous_;
  que fai -- re_? Que fai -- re_?
  Quel con -- seil sub -- til me pro -- po -- \dash ses -- tu_?
  A -- llons a -- llons, Chef de la Ga -- rde,
  j'ai trop be -- soin de toi en ces mo -- ments gra -- ves, très gra -- ves_;
  ex -- trê -- me -- ment graves… __ _
  Pa -- \dash rle -- moi un peu __ _ de cet É -- tran -- ger.
  Je ne sais pas…
  \dash Est -- il vert_?
  \dash A -- \leftSyl t_—_il huit bras_?
  \dash Mange -- \leftSyl t_—_il du feu_?
  \dash Fait -- il…
  vingt pieds de haut_?
  Vingt pieds de la -- _ rge_?
  Vingt pieds de \leftSyl biais_?!
  A -- LORS, à quoi \dash voit -- on que cet É -- tran -- ger
  qui s'est é -- cra -- sé dans sa ma -- chi -- ne vo -- lante __ _
  est un é -- tran -- ger_?
  \dash Est -- ce tout_?
  Raah_! __ _ _ Raah_! __ _ _
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  À ton a -- vis, que \dash dois -- je fai -- re de cet é tran -- ger_?
  \dash C'est -- \dash à -- dire, la -- que -- lle_?
  Oui, oui_;
  mais la -- que -- lle_?
  Ê -- tre Roi… Ê -- tre Roi… Ê -- tre Roi \leftSyl …_c'est ê -- tre seul.
  RAAH_! C'est é -- ner -- vant, très é -- ner -- vant_;
  ex -- trê -- me -- ment é -- ner -- vant.
  Je m'en vais de ce pas voir __ _ cet É -- tran -- ger.
  É -- ner -- vant, très é -- ner -- vant_;
  ex -- trê -- me -- ment é -- ner -- vant.
  É -- ner -- vant, très é -- ner -- vant_;
  ex -- trê -- me -- ment é -- ner -- vant.
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              ACTE I                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%  Premier Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneUnBarytonUnTexte = \lyricmode {
  Aah… __ _ _ _ \freeStyleOn
  \markup \null __ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _ _
  _ _ _ _ _
  _ _ _ _ _
  _ _ _ _ _ _ _ _ _ _ _ _ _ _
  _ \freeStyleOff
  Mmm… __ _ Mmm… __ _ Mmmh… __ _ Aa… __ _  Aaa… __ _ _ _ AAAH_!

  Euh… Oui…
  Je… je ne me sou -- viens plus…
  Euh…__ _ Euh…__ _ ça__ _ non__ _ plus…
  Plu -- tôt au de -- ssus… à la tête, quoi…
  Je ne sais pas…
  Je ne sais PAS_!…

  Je pen -- se… __ _ Je pen -- se… que vo -- tre femme…
  \leftSyl
  …_fait des é -- co -- no -- mies __ _ en ra -- pié -- çant vos a -- ffaires…


  A -- mné -- sique_?! A -- mné -- sique_?!

  A -- mné -- si -- que,  a -- mné -- si -- que, je suis a -- mné -- si -- que_!
  A -- mné -- si -- que,  a -- mné -- si -- que, je suis a -- mné -- si -- que_!
  A -- mné -- si -- que,  a -- mné -- si -- que_! A -- mné -- si -- que_!
  A -- mné -- si -- que_; a -- mné -- si -- que_!
  Ex -- trê -- me -- ment a -- mné -- si -- que_!
}

ActeUnSceneUnSopranoUnTexte = \lyricmode {
  En -- dor -- mi, en -- dor -- mi_; de -- puis mi -- di.
  Non non non, noon_; __ _ il a -- vait dé -- jà pris un gnon… __ _
  Ce sont des pan -- se -- ments_; il sai -- gnait te -- lle -- ment…
  Li -- go -- té, li -- go -- té_? C'est qu'il se dé -- ba -- ttait…
  Mais il est é -- vei -- llé_; je vais l'in -- te -- rro -- ger.

  \ital Hm -- \ital hmm.
  É -- tran -- ger, É -- tran -- ger, est-ce que vous m'en -- ten -- dez_?
  Co -- mmen -- çons, co -- mmen -- çons, quel est donc vo -- tre nom_?
  C'est pas tout, c'est pas tout, a -- lors d'où ve -- \dash nez -- vous_?
  En -- nuy -- eux, en -- nuy -- eux_; a -- \dash vez -- vous mal aux yeux_?
  C'est no -- té, c'est no -- té_; quel est vo -- tre mé -- tier_?
  Di -- \dash tes -- moi, di -- \dash tes -- moi_; pour -- quoi ê -- tes -- vous là_?

  Un __ _ grand bra -- vo
  \leftSyl
  …_pour ces é -- co -- no -- mies de sty -- lo.
  Voy -- ons si ces quel -- ques  o -- bjets
  fe -- ront sur -- gir vo -- tre pa -- ssé…

  Ce -- tte chau -- se -- tte, di -- \dash tes -- moi,
  vous fait pen -- ser à quoi_?
  Quoi, pas __ _ la moin -- dre trace ou sou -- ve -- nir fu -- gace
  de tou -- te vo -- tre vie, tout vous est donc ra -- vi_?
  Vos pre -- miers bi -- be -- rons,
  vos mé -- tiers, vos pa -- trons,
  l'o -- deur de vo -- tre mère, vo -- tre comp -- te ban -- caire_?
  Vos a -- mours, vos  fian -- çailles,
  Vos ho -- raires de tra -- vail, __ _
  les pommes de terre sau -- tées,
  les san -- dwiches au pâ -- té_?

  Mais a -- lors… Mais a -- lors…
  A -- mné -- si -- que_; a -- mné -- si -- que…
  A -- mné -- sique, a -- mné -- sique, il est a -- mné -- si -- que_!

  A -- mné -- si -- que, a -- mné -- si -- que, vous ê -- tes a -- mné -- si -- que_!
  A -- mné -- si -- que, a -- mné -- si -- que, vous ê -- tes a -- mné -- si -- que_!
  A -- mné -- si -- que, a -- mné -- si -- que_!
  A -- mné -- si -- que_!
  A -- mné -- si -- que_; TRÈS a -- mné -- si -- que_!
  A -- mné -- si -- que_! A -- mné -- si -- que_!  Très_—…
  Ex -- trê -- me -- ment… Ex -- trê -- me -- ment… Ex -- trê -- me -- ment…

  C'est très ai -- sé_; u -- sez __ _ du sé -- rum de vé -- ri -- té_!
  Si -- re Si -- re je n'o -- se… vous su -- ggé -- rer l'hyp -- no -- se…
  La tor -- ture_? Quelle tor -- ture_? Ce choix est bien trop dur.
  C'est le temps, oui le temps, le mei -- lleur trai -- te -- ment_!

}

ActeUnSceneUnBarytonDeuxTexte = \lyricmode {
  Co -- mment \dash est -- il_?
  Aah_! __ _ Vous l'a -- vez a -- sso -- mé_!
  Aah_! __ _ Vous l'a -- vez ba -- illo -- né_!
  Ah_; vous l'a -- vez li -- go -- té_!…
  Ses __ _ in -- ten -- ti -- ons nous sont en -- core in -- co -- nnues_;
  c'est em -- bê -- tant, très em -- bê -- tant_; ex -- trê -- me -- ment…

  Mais… Mais…

  A -- mné -- sique_?! A -- mné -- sique_?!

  A -- mné -- si -- que, a -- mné -- si -- que, il est a -- mné -- si -- que_!
  Non, non, __ _ non_; ce -- la ne se peut_!
  A -- mné -- si -- que, a -- mné -- si -- que, il est a -- mné -- si -- que
  \leftSyl
  —_Co -- mment __ _ o -- \dash se -- \leftSyl t_-_il_?
  Co -- mment o -- \dash sez -- vous_?
  A -- mné -- si -- que, TRÈS a -- mné -- si -- que_; EX -- trê -- me -- ment
  a -- mné -- si -- que_!
  A -- mné -- si -- que_; a -- mné -- si -- que_!
  A -- mné -- si -- que_! A -- mné -- si -- que_!
  \leftSyl zA -- mné -- si -- que…
  \leftSyl tA -- mné -- sique_! __ _

  C'est en -- nuy -- eux, très en -- nuy -- eux
  \leftSyl
  —_ex -- trê -- me -- ment en -- nuy -- eux.
  Un a -- mné -- sique_! Qu'y \dash puis -- je, que fai -- _ re_?
  Que dia -- ble_! N'a -- \dash vez -- vous rien de plus sé -- rieux_? Oui_?
  Non non non, non non, nooon… __ _ _
  Je vou -- lais par -- ler…
  \leftSyl …_de tor -- tu -- re_!
  A -- LORS, co -- mment sa -- voir ce qu'il ne sait plus_?
  MAIS de temps, nous n'en a -- vons PAS_!
  Fâ -- cheux… __ _ Très fâ -- cheux… __ _ Ex -- trê -- me -- ment…
  \leftSyl …_Fâ -- cheux.
}

%------------------------------------------------------------------%

ActeUnSceneUnBisBarytonUnTexte = \lyricmode {
  Ma -- jes -- té, \leftSyl —_si je puis me per -- me -- ttre_—
  \dash suis -- je donc le seul __ _ _ é -- tran -- ger
  que vous a -- yez ja -- mais ren -- con -- tré_?
  S'il en é -- tait ain -- si, co -- mment se -- _ \dash rais -- je né_?
  Mais… C'est a -- be -- rrant_!
  Quoi __ _ qu'il en soit, mon des -- tin est en -- tre vos mains_;
  Qu'a -- \dash llez -- vous fai -- _ _ re de moi, __ _ _
  qu'a -- \dash llez -- vous fai -- _ re de moi_?
  Le choix… __ _
  \leftSyl …_le choix est large, e -- ffec -- ti -- ve -- ment…
  Cer -- tains disent __ _ qu'à tra -- vers la mort __ _ _ on a -- tteint
  u -- ne fo -- rme de li -- ber -- té… __ _
  Mais au -- ssi que trop de li -- ber -- té
  tue __ _ la li -- ber -- té.
  U -- ne mort li -- bé -- ra -- tri -- ce…
  U -- ne li -- ber -- té mor -- te -- lle…
}

ActeUnSceneUnBisBarytonDeuxTexte = \lyricmode {
  Un é -- tran -- ger_! En nos murs_!
  Un é -- tran -- ger_! Sous ce toit_!
  Un é -- tran -- ger_! __ _ _
  \ital \dash "(Hm" -- \ital "hmm.)" Il faut… Il faut… Il faut…
  Co -- mment, le seul_?!
  J'es -- pè -- re bien que vous ê -- tes
  le seul é -- tran -- ger au mon -- de_!
  Qu'en \dash sais -- je_; sans doute ê -- \dash tes -- vous
  né d'un PET, d'un CRA -- CHAT,
  et d'un GUA -- NO mis __ _ en -- sem -- _ ble.
  Oui… le gua -- no \leftSyl …_est e -- xa -- gé -- ré.
  Mmm… __ _ mmmh… __ _ _ mmm… __ _
  \leftSyl J'é -- tu -- die un va -- ste plan de po -- ssi -- bi -- li -- tés…
  qui vont de la li -- ber -- té a -- bso -- lue…
  à la mort dé -- _ fi -- _ ni -- tive.
  Mer -- ci de ne plus \leftSyl m'ai -- der.
}
%%%%%%%%%%%%%%%%%%%%%%%%%% Interlude Un %%%%%%%%%%%%%%%%%%%%%%%%%%%%

InterludeUnContraltoTexte = \lyricmode {
  Voy -- ons ça…
  Voy -- ons ça…
  Voy -- ons ça, voy -- ons ça_! \ital Hm.
  A -- rro -- sons ça,
  a -- rro -- sons ça.
  A -- rro -- sons ça…
  Goû -- tons ça_!
  Goû -- tons ça…
  Goû -- tons ça,
  goû -- tons goû -- tons ça… __ _
  Goû -- tons ça,
  goû -- tons ça,
  goû -- \dash ton -- Cra -- chons ça_! \ital Ptt_! \ital Ptt_!
  Cou -- _ pons ça_! __ _
  Cou -- pons ça…
  Cou -- pons ça,
  cou -- pons ça,
  cou -- pons ça,
  cou -- pons ça,
  cou -- pons ça,
  cou -- pons ça…
  Cou -- pons ça…
  Cou -- pons ça,
  cou -- pons ça,
  cou -- pons cou -- pons tout ça…
  Cou -- pons, cou -- pons_; cou -- _ pons ça…
  Cou -- pons ça,
  cou -- pons ça,
  cou -- pons ça… __ _ _
  Ca -- chons ça_!
  Ca -- chons ça_!
  Ca -- chons ça…
  Ca -- chons ça…
  Voy -- ons ça,
  voy -- ons ça.
  Voy -- ons ça…
}

%%%%%%%%%%%%%%%%%%%%%  Deuxième  Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneDeuxSopranoDeuxTexte = \lyricmode {
  %FIXME: \leftSyl doesn't work in this part (wtf?)
  Mmm… __ _ _ _ _ _ _ _ _ _ _ _ _ _
  Mmm… __ _ _ _ _ _ _ _ _ _ _ _ Mm… A -- _ mour… __ _
  Ma main n'a pas a -- ssez de doigts
  pour por -- ter à la fois
  tou -- tes les ba -- gues que j'aime… __ _
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  Aa… __ _ _ Aa… __ _ _ _ _ _ A -- _ mour… __ _
  Mon cou ne peut con -- te -- nir tout… __ _
  il me fau -- drait trois têtes en tout,
  pour tous les co -- lli -- ers, __ _ les di -- a -- dèmes… __ _
  Aaa… __ _ _ _ _ _ _ Mmm… __ _ _ _ _ _ _
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
  A -- _ _ _ _ mour…
  S'en -- ta -- _ _ _ _ ssent en mes a -- ppar -- te -- ments
  les ru -- bis, sa -- phirs, di -- a -- mants_;
  d'a -- sso -- mants mon -- ceaux de gemmes… __ _

  Mon cher, sa -- \dash vez -- vous que la so -- mme de vos sou -- cis
  se lit \dash par -- de -- ssus vos sour -- cils_?
  J'ai la so -- lu -- tion __ _ tou -- te tra -- cée_!
  Dé -- mi -- ssio -- nnez_!
  J'ai ouï di -- _ re __ _ qu'un É -- tran -- ger
  é -- tait tan -- tôt a -- rri -- vé…
  \leftSyl …_é -- _ tran -- _ _ _ ger… \leftSyl …_a -- _ rri -- _ _ _ vé…
  Mmm… __ _ _ _ _ _ _ \leftSyl …_In -- vi -- \dash tons -- le vers vingt heures.
  À dî -- ner. À dî -- ner_!
  D'un dî -- ner, do -- \dash nnons -- nous l'i -- dée
  du sort que vous lui des -- ti -- ne -- rez…

  \ital Ooooh_!!!! Vou -- \dash lez -- vous dire…
  Au -- \dash rait -- il de be -- lli -- queu -- ses en -- vies_?
  Nous des -- ti -- ne -- \dash rait -- il de noirs de -- sseins,
  du -- _ rant __ _ ce dî -- ner __ _ a -- no -- din_?
  Un a -- _ pé -- ri -- tif a -- gre -- ssif_?
  Un plat en for -- me d'a -- tten -- tat_?
  Au fro -- ma -- _ \leftSyl ge_un __ _ car -- na -- ge_;
  et, au __ _ mo -- ment des li -- queurs…
  \leftSyl …_un ou -- trage à la pu -- deur_! __ _

  Dieux_! Mon Dieu… Mon Dieu…
  Quel vin_? Dieux… __ _ Mon Dieu… Mon Dieu_!
  Quelle cui -- sson pour la viande_:
  à point, sai -- gnante, ou bleue_?
  Mon Dieu_! Mon Dieu_!
  Oui, quel de -- ssert \dash vaut -- il mieux_:
  sor -- bet ou crème aux œufs_?
  Mon Dieu, mon Dieu_;
  quels bi -- joux pour mon cou_?
  Ex -- trê -- me -- ment… ca -- ta -- stro -- phi… --
  \leftSyl …_que.
}

ActeUnSceneDeuxBarytonDeuxTexte = \lyricmode {
  Tou -- jours ces pro -- blè -- mes de res -- pon -- sa -- bi -- li -- té;
  ma chè -- re, très __ _ chère é -- pou -- se…
  Oui, oui_?
  Euh… Non, non_; ce… \leftSyl …_ce se -- rait u -- ne trop lou -- _ rde
  res -- pon -- sa -- bi -- li -- té… que de dé -- mi -- ssio -- nner…
  Un É -- tran -- ger, très é -- tran -- ger_; ex -- trê -- me -- ment
  é -- tran -- ger. __ _
  À dî -- ner_? À dî -- ner_?!? À dî -- ner_?…
  D'un dî -- ner do -- \dash nnons -- nous l'i -- dée…
  Oui, oui_; oui, oui oui_! __ _ _

  Mais_! C'est très dan -- ge -- reux __ _ de le faire
  ve -- nir à no -- tre ta -- _ ble_;
  nous ne co -- nnai -- ssons rien de lui…
  Non_! Non,__ _ non, non…
  Voy -- ons, voy -- ons_! Il \leftSyl y_a bien d'au -- tres ques -- tions…
  \dash Est -- il a -- ller -- gique au foie gras_?
  Quel ty -- pe de vin ai -- \dash me -- \leftSyl t_-_il_?
  Nous ri -- squons d'ê -- tre dé -- sas -- treux_!…
  Mon Dieu… Mon Dieu… Quel vin \dash doit -- on ser -- vir_:
  cham -- pagne ou bien mou -- sseux_?
  Mon Dieu… Mon Dieu… La viande_?
  Mon Dieu_! Mon Dieu_!
  Quel de -- ssert \dash vaut -- il mieux_?
  Sor -- bet ou crème aux œufs_?
  Mon Dieu, __ _ mon Dieu_! __ _
  Quels bi -- joux pour mon c_-?!
  En -- fin bref. Ce dî -- ner peut de -- ve -- nir
  ca -- ta -- stro -- phi -- que_; très
  ca -- ta -- stro -- phi -- que…
}

%%%%%%%%%%%%%%%%%%%%%%%%% Interlude Deux %%%%%%%%%%%%%%%%%%%%%%%%%%%

InterludeDeuxContraltoTexte = \lyricmode {
  Voy -- ons ça…
  Voy -- ons ça,
  voy -- ons ça_!
  Voy -- \dash on --
  Cou -- pons ça,
  cou -- pons ça_!
  Cou -- pons __ _ ça_!…
  Cou -- pons __ _ ça…
  Cou -- pons ça_!
  Cou -- pons ça_!
  Cou -- pons ça,
  cou -- pons ça,
  cou -- pons cou -- pons cou -- pons cou -- pons cou -- pons ça_!
  Ca -- chons ça…
  Ca -- chons ça…
  Brû -- chons ça, __ _ brû -- lons ça_! __ _
  Aah_! Aaah_!! __ _
  É -- tei -- gnons ça_!
  \dash É -- \dash tei -- \dash gnons -- \dash ça.
}

%%%%%%%%%%%%%%%%%%%%%% Troisième Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneTroisTenorTexte = \lyricmode {
  É -- tran -- ger…
  É -- tran -- ger, É -- tran -- ger_!
  Le Roi vou -- drait sa -- voir si vous pré -- fé -- rez
  le chaud ou le froid… \leftSyl …_et au -- ssi
  si vous ai -- mez quand ça pi -- que.
  Mais… quoi_?
  Non, non… __ _ Ce n'est pas… __ _
  Vous voi -- là tout pâle…
  Vous trou -- \dash vez -- vous mal_?
  É -- cou -- _ tez -- moi_!
  Vous me com -- pre -- nez mal_!
  Pas du t…
  Il est ques -- tion de m…
  Ho -- rreur_! Ho -- rreur, ho -- rreur_!
  Il com -- prend tout de tra -- v…
  Ne -- nni_! Ne -- nni, ne -- nni_!
  Je me suis mal ex -- pri -- m…
  Non_!
  Non non non non non_; non non non non non non non non non_!
  Non, non_; non non…
  \leftSyl …_C'est pour le dî -- ner au -- quel il veut vous con -- vi -- er.
  Ou au moins, dî -- ner a -- vec vous a -- vant de vous e -- xé -- cu -- ter.
  Je ne sais pas, je ne sais pas_;
  en tout cas, il ne m'a pas in -- vi -- té à la mise à mort et tout ça…
  Ah_! ça oui_;
  que -- lle te -- rri -- ble cho -- se_;
  c'est a -- tro -- ce, a -- tro -- _ ce_!
  \leftSyl …_gâ -- cher de la nou -- rri -- ture
  pour quel -- qu'un qui va mou -- rir…
}

ActeUnSceneTroisBarytonUnTexte = \lyricmode {
  Il hé -- si -- te peut -- être à me brû -- ler vif_;
  ou à me con -- ge -- ler, puis bri -- ser mon corps en pu -- blic…

  Le pal_! __ _ Le pal_! __ _
  Le pal_! __ _ Le pal_! __ _
  Non, non_! __ _ Pas le pal_! __ _
  Le pal_! __ _ _ _ Le pal_! __ _ _ _
  Non_! __ _ Tout mais pas le pal_!
  Sans dou -- te le Roi \dash veut -- il m'être a -- gré -- able,
  en me lai -- ssant choi -- _ sir ma mort…
  Tout, sauf le pal. Ma mort, par le pal. Quelle ho-
  Vers que -- lle mort tour -- men -- tée le Roi \dash va  --
  \leftSyl t_-_il m'en -- traî -- ner_?
  Mes heures sont comp -- tées.
  Vou -- \dash lez -- vous dire qu'il veut
  m'e -- xé -- cu -- ter lors d'un dî -- ner_?
  En ê -- \dash tes -- vous sûr_?
  Il sait que vous ê -- tes un ho -- mme ra -- ffi -- né
  et que le spec -- ta -- cle vous ho -- rri -- fie -- rait… __ _
}

%------------------------------------------------------------------%

ActeUnSceneTroisBisBarytonDeuxTexte = \lyricmode {
  Mmm… __ _ _ _ _ _ _ _
  \leftSyl …_mon Dieu… Dieu_!… Dieu…
  Dieu, Dieu. Vous tom -- bez bien, très bien_;
  ex -- trê -- me -- ment bien…
  J'ai u -- ne ques -- tion à vous po -- ser.
  \leftSyl …_Et… ce -- tte bou -- rse, pou -- \dash rrait -- e -- lle
  fai -- re l'a -- ffaire_?
  J'ai là… u -- ne __ _ ques -- tion gra -- ve, très gra -- ve_;
  ex -- trê -- me -- ment gra… __ _
  Un É -- tran -- ger s'est é -- cra -- sé sur le châ -- teau_;
  que dois -- je fai -- re de lui_? __ _
  Main -- te -- nant, __ _ il me sem -- ble
  me sou -- ve -- nir pour -- quoi je ne m'é -- tais pas
  ré -- a -- bo -- nné cette a -- nnée…
}

ActeUnSceneTroisBisContraltoTexte = \lyricmode {
  Un sou -- ci, mon a -- mi_?
  Un in -- stant, s'il vous plaît…
  Vous ê -- \dash tes -- vous ré -- a -- bo -- nné
  à ma re -- li -- gion cette a -- nnée_?
  Voy -- ons ça… Non… __ _ Non… __ _
  Non, __ _ non __ _ non… __ _
  Vous ne l'ê -- tes pas_;
  non, __ _ non, __ _ non, __ _ non, __ _ n…
  A -- ssu -- ré -- ment, a -- ssu -- ré -- ment_;
  nous a -- vons, je le vois,
  tous deux foi en l'ar -- gent.
  Oui, oui_; mon temps __ _ est pré -- cieux, très cher_;
  à l'e -- ssen -- tiel, vou -- \dash lez -- vous_!
  Bra -- vo, bra -- vo_; bra -- vo, bra -- vo_:
  co -- mme c'est bien dit…
  Fé -- li -- ci -- ta -- ti -- ons
  pour vo -- _ tre con -- ci -- si -- on.
  A -- dre -- ssez vo -- tre re -- quête
  au pla -- fond, et é -- cou -- tez ré -- pon -- dre
  vo -- tre cœur…
}

%------------------------------------------------------------------%

ActeUnSceneTroisTerSopranoUnTexte = \lyricmode {
  \leftSyl
  …_Et ce soir, et ce soir, que de -- vient vo -- tre mé -- moire_?
  Le sou -- ve -- nir to -- tal se pro -- vo -- que sou -- vent
  par un __ _ vio -- lent choc.
  Sui -- \dash vez -- moi, sui -- \dash vez -- moi_;
  ce -- ci vous ai -- de -- ra…

  \leftSyl
  …_Ce sou -- ve -- nir est vô -- _ tre_;
  mais n'y -- \dash a -- \leftSyl t_-_il rien d'au -- tre_?

  Si je vous mon -- tre ce -- tte ta -- che,
  est-ce qu'un sou -- ve -- nir plus ré -- cent se dé -- ta -- che_?
  Et là_? Et là_? Et là_? Et…
  \leftSyl
  …_Et a -- vec ce nu -- a -- _ _ ge_?
  Vos e -- fforts  sont mau -- vais_; main -- te -- nant je m'en vais.

}

ActeUnSceneTroisTerBarytonUnTexte = \lyricmode {
  Pas mieux __ _ que cet a -- près_- mi -- di.
  Vous au -- ssi, vous vou -- lez me ta -- per, me tu -- er_?

  Oh_!…
  Oh… __ _
  …_Mais… ce sont des mor -- ceaux de…
  Oh… Oui… Je… je me sou -- viens… C'est te -- rri -- _ ble…
  J'a -- vais huit ans.
  J'ai sau -- té __ _ sur la ta -- ble du sa -- lon…
  E -- lle s'est __ _ e -- ffon -- drée…
  \leftSyl
  …_et ma mère m'a do -- nné ma pre -- miè -- re fe -- ssée.
  Rien… __ _ Non… Non. Non_; non, non. __ _
  Non, non, non_; __ _ non, non_! __ _ _
  Pas da -- van -- ta -- _ ge_!

  Aah…
  Là, vous me ra -- ppe -- lez mon an -- cie -- nne femme.
}

%%%%%%%%%%%%%%%%%%%%  Quatrième  Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneQuatreSopranoDeuxTexte = \lyricmode {
  %FIXME: lefSyl doesn't work here.
  É -- tran -- ger, vo -- tre tra -- chée __ _ \dash est -- e -- lle nou -- ée_?
  Vo -- tre bou -- _ che ti -- re -- bou -- cho -- nnée_?
  Vo -- tre go -- rge, en -- gor -- gée, vo -- tre glotte… __ _
  en -- glou -- tie_? __ _
  En -- fin quoi_! Pas le moin -- dre son de -- puis le bou -- illon_;
  pas un mot de -- puis le gas -- pa -- cho_!
  Nous a -- llons tous mou -- rir un jour_;
  a -- lors pour -- quoi ce soir se taire, et fai -- re
  si -- len -- ce jus -- qu'au de -- ssert_?
  \ital Qu -- \ital oi_?! Qu'en -- \dash tends -- je_? U -- ne mise à mort au
  ti -- ra -- mi -- su_?! Du sang sur les sor -- bets ci -- tron
  ai -- gre -- doux_?! U -- ne dé -- co -- lla -- tion a -- près la
  co -- lla -- tion_? Mon con -- vive é -- cor -- ché vif_?
  Mon in -- vi -- té dé -- chi -- que -- té_? __ _
  Ah_! Me voi -- là ra -- ssu -- rée. Si e -- xé -- cu -- ti -- on
  il y a -- vait, j'au -- rai vou -- lu con -- vi -- er mes a -- mies
  du club de bro -- de -- rie.
  A -- mné -- si -- que… A -- mné -- si -- que…
  A -- mné -- sique, a -- mné -- si -- que, ça c'est co -- mi -- que.
  Peut -- être ê -- \dash tes -- vous… un tu -- eur sa -- di -- que_;
  un é -- gor -- geur é -- ga -- ré, un vio -- leur vi -- tri -- o -- leur
  ou… un vo -- leur de va -- leurs… __ _
  Ce -- tte der -- nière hy -- po -- thè -- se
  me met très mal à l'aise.
  Mon dieu_!
  Mon dieu_! Aah_!
  Fâ -- cheux, très __ _ fâ -- cheux_; ex -- trê -- me -- ment fâ -- cheux.
  Non_! Non_! Non_! Non, non_! Non, non_! Non_! Non_! Non.
  J'a -- ppré -- cie fort bien son vi -- sa -- ge fin et ra -- cé.
  Je trou -- ve mer -- vei -- lleux ses che -- veux on -- du -- leux. Ah_?
  \leftSyl …_La ma -- gie du my -- stè -- re…
  Mon bon, nul en com -- pa -- rai -- son n'au -- ra votre é -- lé -- gant
  \leftSyl …_men -- ton.
}

ActeUnSceneQuatreBarytonUnTexte = \lyricmode {
  Un ho -- mme qui va mou -- rir et qui par -- le trop
  est soit soûl, soit fou…
  Je ne sais pas, je ne sais pas… Peut -- ê -- tre par -- ce que
  le re -- pas ter -- mi -- né, e -- lle se -- ra tran -- chée, ma
  tra -- chée_? Ou… Ou bien ma gor -- ge, é -- gor -- gée…
  Ou… \leftSyl …_mon cou, cou -- pé… __ _
  Oui_! C'est ça_! __ _ Ou… __ _
  Ou peut -- ê -- tre que je suis Dieu…
  et que je vous ai tous -- \ital seuh cré -- és -- \ital euh.
  \leftSyl …_sur -- tout si je suis Dieu ET
  un tu -- eur sa -- di -- que, é -- gor -- geur et __ _ vi -- o -- leur…
  Peut -- ê -- tre \dash peut -- il… __ _
  me cou -- per les on -- gles…
  Non  non __ _ non, __ _ non non non non_?
  Non non non_? Non non non_?
  Non  non __ _ non_? __ _ Non  non __ _ non_? __ _
  Non, non_? Non, non_? Non_? Non_? Non_?
  Oh_! Je me sou -- viens_!…
  Le soir a -- vant de me cou -- cher,
  je bois tou -- jours un verre de lait.
  La cui -- sine… Où est la cui -- si -- ne_?
  Mais pas du tout_; je veux juste un verre de lait.
  Oh. Ce sont de bo -- nnes i -- dées_; je n'y a -- vais
  pas pen -- sé… Je di -- rai au Roi qu'e -- lles
  vie -- nnent de vous, ce -- la va de soi.
  Oui, je pou -- rrais l'é -- tri -- per… Ça me sem -- _ ble par -- fait.
  Un ve -- rre de lait m'y ai -- de -- rait. Mer -- ci.
}

ActeUnSceneQuatreBarytonDeuxTexte = \lyricmode {
  Mais_! __ _ Pas du tout_! Du tout_! Non, non_; je n'ai pris
  au -- cu -- ne dé -- ci -- sion.
  Mais pour l'in -- stant, rien n'est dé -- ci -- dé.
  L'É -- tran -- ger est
  a -- mné -- si -- que, très a -- mné -- si -- que_;
  ex -- trê -- me -- ment a -- mné -- sique…
  Mon dieu_! Mon dieu_! Dieu_!
  Dieu, Dieu, __ _ Dieu_! __ _ _
  Cet É -- tran -- ger sou -- tient qu'il pou -- rrait ê -- tre dieu_;
  ce -- la se -- rait fâ -- cheux, très fâ -- cheux_;
  ex -- trê -- me -- ment fâ -- cheux.
  \ital Ah, \ital non_! Gaaaaa -- rde_! Chef de la Gaaaaa -- rde_!
  Ren -- vo -- yez cet É -- tran -- ger en pri -- son, il me
  fait per -- dre la rai -- son.
  Non, non, __ _ non… Je dois ré -- flé -- chir à tout ça.
  Non, non, __ _ non_; non non non non…
  Non, non, __ _ non_; non non non non…
  Non  non __ _ non, __ _ non non non non…
  Non  non __ _ non, __ _ non non_;
  non  non __ _ non, __ _ non non_;
  non  non __ _ non, __ _ non non_;
  non  non __ _ non, __ _ non non_;
  non  non __ _ non, __ _ non non_;
  non  non __ _ non, __ _ non non_;
  non_! Non_! Non_! Non.

  Ma chère… Que pen -- \dash sez -- vous de cette a -- ffai -- re_?
  Mais_! Il a des ban -- da -- ges par -- tout sur le vi -- sa -- ge_!!?
  Ban -- da -- ges_! Ban -- da -- ges_!! Par -- tout sur le vi -- sa -- ge_!!!
  Pre -- nez garde __ _ à ma ja -- lou -- sie, ma chère.
}

ActeUnSceneQuatreTenorTexte = \lyricmode {
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- té_;
  or -- do -- nnez, or -- do -- nnez, or -- do -- nnez_;
  j'o -- bé -- i -- rai, j'o -- bé -- i -- rai, j'o -- bé -- i -- rai_!
  Et main -- te -- nant, \dash puis -- je le tuer_?
  \dash Puis -- je lui cou -- per un bras_?
  \dash Puis -- je lui cou -- per un doigt_? Mais…
  Non non __ _ non, __ _ non non non non_!
  Non non __ _ non, __ _ non non
  non non __ _ non, __ _ non non
  non_! Non non non_! Non non non_! Non_! Non_!
  \leftSyl …_d'u -- ne pri -- se de ka -- ra -- té_?
  Là, vous n'en au -- rez pas.
  A -- lerte_! A -- lerte_! à l'é -- va -- sion_!
  L'É -- tran -- ger veut s'é -- va -- der.
  Mon œil, par -- di_! Peut -- ê -- tre vou -- \dash liez -- vous
  a -- ssa -- ssi -- ner le Roi, ou me -- ttre le feu i -- \dash ci -- bas_!
  Non non __ _ non, non non non non non_; non non non non non non non_;
  le Roi en fait é -- tri -- per pour moins que ça…
  Aaah_! Non non non_; je n'ai rien dit. Vous… Vous…
  Vous a -- llez ê -- tre bien gen -- til, et sa -- ge -- ment
  a -- ller au lit.
}

ActeUnSceneQuatreContraltoTexte = \lyricmode {
  Oui_? Fâ -- cheux, très __ _ fâ -- cheux_; ex -- trê -- me -- ment fâ -- cheux.
  Hmm… A -- dre -- ssez vo -- tre re -- quête au pla -- fond et é -- cou -- tez
  ré -- pon -- dre vo -- tre cœur…
  Non non __ _ non, __ _ non non non non_!
  Non non non non_!
  Non non non non_!
  Non, non_! Non, non_! Non_! Non_!
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            ENTR'ACTE                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EntracteSopranoUnTexte = \lyricmode {
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux…
  Une a -- mné -- sie ne gué -- rit que si
  le pa -- tient sait pa -- tien -- ter.
  Un __ _ tel cas __ _ peut pren -- dre des mois_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment __ _ en -- nuy -- eux_!

  Psy -- chia tri -- que, psy -- chia -- tri -- que,
  un beau cas psy -- chia -- tri -- que_;
  neu -- ro -- lo -- gi -- que, phar -- ma -- ceu -- ti -- que,
  psy -- cho -- so -- ma -- ti -- _ que_;
  scien -- ti -- fi -- _ que, scien -- ti -- fi -- _ que,
  je suis un scien -- ti -- fi -- que_;
  po -- li -- ti -- que, po -- li -- ti -- que,
  pas un homme po -- li -- ti -- que.

  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  Dans un an. Chau -- ssette…
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy_-.

  \dash Po -- om, \dash po -- om, \dash po -- om pom
  \dash po -- om, \dash po -- om_-
  Mmm… __ _ _ Mmm… __ _ _ _
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om, pom pom.
  Pom, \dash po -- om pom, pom pom, pom pom…

  Pom, pom, \dash po -- om Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om Pom, pom.
  Pom pom pom, pom pom pom, pom pom pom,
  pom \dash po -- om pom pom pom…
  Pom pom pom pom, \dash po -- om pom pom pom pom,
  pom pom \dash po -- om \dash po -- om
  \dash po -- om \dash po -- om, pom.
  Pom… __ _ Pom… __ _ POM… __ _ POM…
}

EntracteSopranoDeuxTexte = \lyricmode {
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- _ eux…
  Mes co -- ffrets, mes boî -- tiers, sont tous
  pleins à cra -- quer… Je ne sais où ran -- ger
  ce tout pe -- tit co -- llier_:
  en -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment __ _ en -- nuy -- eux_!

  Es -- thé -- ti -- que, es -- thé -- ti -- que,
  j'ai -- me cette es -- thé -- ti -- que_;
  ma -- gni -- fi -- que, ma -- gni -- fi -- que,
  ces pierres sont ma -- gni -- fi -- ques,
  au -- then -- ti -- ques, au -- then -- ti -- ques,
  des dia -- mants au -- then -- ti -- ques.

  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  \leftSyl …_dans mon coffre à bi -- joux_?
  Je les ai -- me sur ma tête.
  Je les ai -- me sur mes doigts.
  Quelle ho -- rreur, quelle ho -- rreur_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy_-.

  \dash Po -- om, \dash po -- om \dash po -- om pom pom
  \dash po -- om, \dash po -- om \dash po -- om pom pom
  \dash po -- om, \dash po -- om_-

  Mmmm… __ _ Mmm… __ _ _ Mmm… __ _
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om, pom pom.
  Pom, \dash po -- om pom, pom pom,
  \dash po -- om \dash po -- om pom…
  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom, pom.
  Pom, pom, \dash po -- om
  Pom pom pom, pom pom pom,
  pom, pom pom pom pom pom,
  pom pom pom, pom pom pom,
  pom pom pom, \dash po -- om \dash po -- om pom,
  pom pom pom, pom pom pom.
  \dash Po -- om… \dash Po -- om… POM… __ _ POM…
}

EntracteContraltoTexte = \lyricmode {
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om_!
  Pom, pom_! Pom_!
  \dash Po -- om…
  \dash Po -- om.
  \dash Po -- om.
  Pom pom, pom, pom, \dash pom -- pom pom pom
  Pom pom, pom, pom, \dash pom -- pom pom pom
  Pom pom, \dash pom -- Pom pom, \dash pom_-
  Pommmm… __ _ _ _
  Pommmm… __ _ _ _

  Pom, \dash po -- om
  pom, \dash po -- om
  Po -- o -- om pom…

  Pom pom pom, __ _
  pom pom pom…
  pom pom pom, __ _
  pom. __ _
  Pom, pom, pom, pom, \dash po -- om pom pom
  pom, pom. \dash Po -- om \dash po -- om \dash po -- om.
  \dash Pom pom pom…
  Pom… __ _
  Pom pom pom pom pom pom POM… __ _
  POM…
}

EntracteTenorTexte = \lyricmode {
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  Une bonne ex -- é -- cu -- tion_!
  Pen -- dai -- son_! É -- lec -- tro -- cu -- tion_!
  Je -- té du haut du châ -- teau_?
  Cou -- pé en pe -- tits mor -- ceaux_?
  Le choix est bien trop vaste
  pour __ _ tu -- er __ _ cet é -- tran -- ger_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment __ _ en -- nuy -- eux_!

  Hé -- mo -- rra -- gi -- que, hé -- mo -- rra -- gi -- _ que,
  une mort hé -- mo -- rra -- gi -- que_;
  é -- lec -- tri -- _ que, é -- lec -- tri -- que,
  ou la chaise é -- lec -- tri -- que.
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  Le fai -- re tom -- ber…
  \leftSyl …_a -- vec des coups de hache_!
  \leftSyl …_des coups de mar -- teau_!
  \leftSyl …_des coups de…
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy_-.

  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om_?
  \dash Po -- om, \dash po -- om,
  \dash po -- om, \dash po -- om,
  \dash po -- om, \dash po -- om,
  \dash po -- om, \dash po -- om,
  \dash po -- om, \dash po -- om,
  \dash po -- om, \dash po -- om, \dash po -- om pom pom,
  \dash po -- om, \dash po -- om, \dash po -- om pom pom,
  \dash po -- om, \dash po -- om, \dash po -- om pom pom,
  \dash po -- om, \dash po -- om, \dash po -- om pom pom,
  \dash po -- om, \dash po -- om_-

  Mmm… __ _ Mmm… __ _ _ _ _
  \dash Po -- om. \dash Po -- om.
  \dash Po -- om, pom pom.
  Pom, \dash po -- om pom, \dash po -- om
  pom, \dash po -- om \dash po -- om…

  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom pom pom pom pom pom
  \dash po -- om pom pom, \dash po -- om pom,
  \dash po -- om \dash po -- om, \dash po -- om pom pom,
  pom pom pom, pom pom pom,
  pom \dash po -- om pom pom pom… __ _
  Pom pom pom, \dash po -- om \dash po -- om \dash po -- om.
  \dash Pom pom pom pom pom pom POM… __ _ POM…
}

EntracteBarytonUnTexte = \lyricmode {
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  Ce Roi sans foi ni loi ne cher -- che -- ra qu'u -- ne cho -- se
  ma foi, c'est le moy -- en de __ _ m'e -- xé -- cu -- ter.
  La li -- ber -- té, ça je le sais,
  ja -- mais je ne l'ob -- tien -- drai_;
  en -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment __ _ en -- nuy -- eux_!

  A -- mné -- si -- que, a -- mné -- si -- que,
  pour -- quoi suis_-_je a -- mné -- si -- que_;
  sym -- pa -- thi -- que, sym -- pa -- thi -- _ que,
  per -- sonne n'est sym -- pa -- thi -- que_;
  dra -- ma -- ti -- que, dra -- ma -- ti -- _ que,
  en cette heure dra -- ma -- ti -- que_;
  fa -- ti -- di -- que, fa -- ti -- di -- _ que,
  ce mo -- ment fa -- ti -- di -- que_;
  lu -- na -- ti -- que, lu -- na -- ti -- _ que,
  car ce Roi lu -- na -- ti -- que_;
  ty -- ra -- nni -- que, ty -- ra -- nni -- _ que,
  m'a l'air bien ty -- ra -- nni -- que.

  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  Il faut que je m'é -- cha -- ppe,
  que j'ou -- vre ce -- tte po -- rte_!
  Quelle ho -- rreur, quelle ho -- rreur_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy_-.

  \dash Po -- om.
  \dash Po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om_-
  Mmm… __ _ _ _ _ _
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om, pom pom.

  Pom, \dash po -- om pom, pom
  \dash po -- om, __ _ pom…
  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om
  Pom, \dash po -- om pom pom
  \dash po -- om \dash po -- om pom pom
  \dash po -- om pom pom pom, pom pom
  pom, pom, pom pom, pom, pom pom, \dash po -- om pom pom
  pom, \dash po -- om \dash po -- om
  pom pom pom \dash po -- om \dash po -- om pom,
  pom pom pom, pom pom pom.
  Pom pom pom pom pom pom pom, pom, \dash po -- om
  POM… __ _ POM…
}

EntracteBarytonDeuxTexte = \lyricmode {
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux.
  Que fai -- re_? C'est un my -- stè -- re.
  Que faire, my -- stère, que fai -- _ rRREUAAH_!
  Cet É -- tran -- ger a le don de tout com -- pli -- quer.
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment __ _ en -- nuy -- eux_!

  Pa -- ci -- fi -- que, pa -- ci -- fi -- que,
  il a l'air pa -- ci -- fi -- que_;
  co -- lé -- ri -- que, co -- lé -- ri -- que,
  je suis trop co -- lé -- ri -- que_;
  dra -- ma -- ti -- que, dra -- ma -- ti -- que,
  ce choix est dra -- ma -- ti -- que_;
  hi -- sto -- ri -- que, hi -- sto -- ri -- que,
  d'une po -- rtée hi -- sto -- ri -- que_;
  é -- ner -- gi -- que, é -- ner -- gi -- que,
  je dois être é -- ner -- gi -- que.

  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy -- eux_!
  \leftSyl …_s'en dé -- ba -- rra -- ser
  i -- mmé -- di -- a -- te -- ment_!
  Quelle ho -- rreur_…
  En -- nuy -- eux, très en -- nuy -- eux_;
  ex -- trê -- me -- ment en -- nuy_-.

  \dash Po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om, pom pom,
  \dash po -- om, \dash po -- om_-
  Mmm… __ _ _ _ _ _
  \dash Po -- om.
  \dash Po -- om.
  \dash Po -- om, pom pom.
  Pom, \dash po -- om pom, pom pom, pom pom…

  Pom, pom, \dash po -- om Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om Pom, pom, \dash po -- om
  Pom, pom, \dash po -- om Pom, \dash po -- om pom,
  pom, pom, \dash po -- om pom, \dash po -- om pom,
  pom, \dash po -- om pom pom pom, pom, pom,
  \dash po -- om pom pom, pom pom pom, pom pom pom,
  pom \dash po -- om pom, \dash po -- om \dash po -- om pom.
  Pom pom pom pom pom pom pom, pom, pom, POM… __ _ POM…
  JE VAIS LE TUER_!!! __ _
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             ACTE II                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%  Premier Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneUnSopranoUnTexte = \lyricmode {
  Tout à fait, __ _ _ Ma -- je -- sté.
  Le cha -- rme no -- stal -- gique __ _
  d'un tas de gra -- vats.
  Tom -- ber du ciel n'est pas vo -- ler.
  Su -- ppu -- ta -- tions… __ _ Manque d'in -- for -- ma -- tion… __ _
  Fai -- sons, fai -- sons u -- ne re -- con -- sti -- tu -- ti -- on_!
  Do -- nnons, do -- nnons u -- ne chance à la science_!
  É -- tu -- dions ça_!
  É -- tu -- dions ça_!
  É -- tu -- dions ça_! __ _
  É -- tu -- dions ça_!
  É -- tu -- dions ça_!
  É -- tu -- dions ça, é -- tu -- dions ça_! __ _ _
  É -- tu -- dions ça…
  É -- tu -- dions ça…
}

ActeDeuxSceneUnSopranoDeuxTexte = \lyricmode {
  \dash Sont -- ce là les re -- stes é -- ta -- lés…
  de la ma -- chi -- ne vo -- lan -- te de __ _ l'é -- _ tran -- _ ger_?
  Que -- lle char -- man -- te ma -- chi -- ne pour se ca -- sser l'é -- chi -- ne.
  Cou -- _ leurs __ _ _ _ cha -- to -- yan -- tes,
  cou -- _ _ rbes cha -- vi -- ran -- tes…
  Cro -- \dash yez -- vous qu'il vo -- lait, pla -- nait, flo -- ttait en
  su -- spen -- si -- on sans
  se sou -- ci -- er de la di -- rec -- tion_?
  Non, non, non. Je veux croire en cette hi -- stoire.
  Le vol __ _ _ des __ _ oi -- _ seaux __ _ vi -- re -- vol -- tant __ _ est si beau.
  Il bri -- sait l'a -- zur, j'en suis sûre, __ _ et di -- stan -- çi -- ait
  l'ho -- ri -- zon. __ _ Il a -- llait là où
  nos re -- gards, pri -- so -- nniers,
  ha -- gards, ja -- mais ne por -- te -- ront. __ _
  Mais… Si ça… __ _ Sauf si ça __ _ sert…
  É -- tu -- dions ça, \ital puis brû -- lons ça_;
  é -- tu -- dions ça, puis brû -- lons ça…
  Sauf si ça sert à vo -- ler là -- bas.
  Sauf si ça sert… à vo -- ler là -- bas… __ _
}

ActeDeuxSceneUnContraltoTexte = \lyricmode {
    É -- lu -- cu -- bra -- tions… __ _
    Pure i -- ma -- gi -- na -- tion. __ _
    Il faut, il faut que nous le brû -- li -- ons_!
    Ce qui n'est pas ré -- per -- to -- rié est
    l'œu -- _ vre d'un cer -- veau -- vi -- cié.
    Brû -- lons ça_! __ _
    Brû -- lons ça_! __ _
    Brû -- lons ça_!
    Brû -- lons ça_! __ _
    Brû -- lons ça_! __ _
    Brû -- lons ça_! __ _
    Brû -- lons ça, brû -- lons ça_! __ _ _
    Puis brû -- lons ça.
    Puis brû -- lons ça.
}

%-------------------------------------------------------------------

ActeDeuxSceneUnBisBarytonDeuxTexte = \lyricmode {
  É -- tran -- ger, bo -- nne nou -- velle, très bo -- nne nou -- velle_;
  ex -- trê -- me -- ment bo -- nne nou -- velle.
  J'ai dé -- ci -- dé de vous é -- xé -- cu -- ter de  -- main en ma -- ti -- née.
  Qui a la chan -- ce de co -- nnaî -- tre  son sort, et la da -- te de sa mort_?
  U -- ne bo -- nne nou -- velle_? Très bo -- nne nou -- velle_?
  Ex -- trê -- me -- ment bo -- nne nou -- velle_!…
  Aaah_! __ _ Aah_! \dash Ah -- ah_! \dash Ah -- aah_!
  J'ai pris u -- ne nou -- ve -- lle dé -- ci -- sion
  con -- cer -- nant vo -- tre mort_!
  J'ai dé -- ci -- dé…
  ON N'IN -- TERR -- ROMPT PAS LE ROI_!
  JE SUIS LE ROI DE TOUT.
  JE SUIS LE ROI DE TOUT.
  JE SUIS LE ROI DE TOUT_! __ _
  TAI -- \dash SEZ -- VOUS_!
  Il se sou -- vient… __ _
  Et moi, je me sou -- viens de vous châ -- tier en -- co -- re plus fort_;
  je vais…
}

ActeDeuxSceneUnBisBarytonUnTexte = \lyricmode {
  Vous me ren -- dez ma li -- ber -- té_?
  Et… C'est une… bo -- nne nou -- velle_?
  A -- lors, en ce cas, j'ai pour vous u -- ne bo -- nne nou -- velle,
  très bo -- nne nou -- velle_; ex -- trê -- me -- ment __ _ bo -- nne nou -- velle_!…
  Vous a -- vez ga -- gné… un coup de poing gra -- tuit.

  E -- ssay -- ez… E -- ssay -- ez, et vous ve -- rrez.
  Un pe -- tit coup. Un pe -- tit coup de rien du tout.
  Ce -- tte dé -- ci -- sion, je m'en fi -- _ che_; je m'en fi -- _ che_;
  je m'en fi -- _ che_; je m'en fi -- _ che_; je m'en fi -- _ che_; je…
  Je m'en fi -- _ che,  je m'en fi -- _ che_!
  Vous n'ê -- tes pas mon roi.
  Le rien de rien du tout.
  Le rien de rien du tout.
  Le rien de rien du tout_! Les nu -- a -- ges \dash eux -- même
  ne vous o -- bé -- i -- ssent pas_; moi, quand je vo -- lais,
  je gli -- ssais par -- mi eux_; j'en fai -- sais mes a -- mis_;
  j'a -- llais en leur coeur et re -- ssor -- tais i -- rra -- dié
  de bo -- nheur… Oh. Je… Je me sou -- viens.
  Je me sou -- viens… __ _
  A -- \dash llez -- y_! Ta -- \dash pez -- le_!
  L'o -- cca -- sion est u -- ni -- que…
  A -- lors, ce se -- ra moi_!
}

ActeDeuxSceneUnBisTenorTexte = \lyricmode {
  On a dé -- cou -- vert un nou -- veau moy -- en
  pour é -- ven -- trer quel -- qu'un_?
  U -- ne bo -- nne nou -- velle_? Très bo -- nne nou -- velle_?
  Ex -- trê -- me -- ment bo -- nne nou -- velle_!…
  A -- lors ça… A -- lors ça… A -- lors ça, je ne vous ai -- me pas,
  mais a -- lors ça_; c'est cu -- lo -- tté.
  Fra -- pper un su -- pé -- ri -- eur est un a -- _ cte… a -- nar -- chi -- ste_!
  A -- _ bject_! __ _ Ré -- pu -- _ gnant_! __ _
  \leftSyl Rrré -- _ pré -- _ hen -- _ si -- _ _ ble_! __ _ _
  Mais… Pour -- tant… Ça doit pro -- cu -- rer un sen -- ti -- ment de
  pui -- ssance ex -- trê -- me -- ment grand, di -- \dash tes -- moi…
  Oui, di -- \dash tes -- moi… __ _
  L'es -- pa -- ce d'un in -- stant, on doit se croi -- re Roi. __ _ Ou mê -- me
  \ital Dieu, qui sait… __ _ Di --  \dash tes -- moi… Oui, di -- \dash tes -- moi…
  Hein_? Qui_? Moi_? Non… MOI_?!! Non, non __ _ non, non non non non…
  Ho -- la -- laa_! Ho -- la -- la -- laaa… __ _
  AAAH_! __ _ C'est lui_! C'est lui_! Ma -- jes -- té, c'est l'É -- tran -- ger_!
  Il vous a bou __ _ rré de coups de pied.
  Ce -- tte dé -- ci -- si -- on se -- ra d'u -- ne beau -- té ba -- roque,
  à l'i -- ma -- _ ge roy -- a -- le de mon fort es -- ti -- mé mo -- narque…
  Tai -- \dash sez -- vous_! Pe -- tit de rien du tout.
  Il se sou -- vient… __ _
  Oooh_!… Ça a l'air ru -- de -- ment fa -- ci -- le…
  Nnnn… Non, non_; im -- po -- ssi -- ble, non…
  Non, non non_; rien que d'y pen -- ser,
  j'ai en -- vie de me mettre en pri -- son.
  Aaah_! Quelle si -- tua -- tion a -- ffreu -- se,
  très a -- ffreuse_; ex -- trê -- me -- ment a -- ffreuse_!
  Je sau -- ve le Roi, et il ne le voit mê -- me pas_!
}

%%%%%%%%%%%%%%%%%%%%  Deuxième Tableau  %%%%%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneDeuxSopranoUnTexte = \lyricmode {
  Vous n'ê -- tes pas ré -- ta -- bli, vo -- tre tête est meur -- trie.
  Un ban -- dage, un ban -- dage \leftSyl …_se -- rait plus sage.
  Fi -- ni -- ssez, fi -- ni -- ssez…
  Fi -- ni -- ssez ce pan -- se -- ment… J'ai
  de nom -- breux pa -- tients.
  La ma -- chine, la ma -- chine, a des lé -- sions bé -- nignes.
  Cet en -- gin __ _ sou -- ffre d'an -- gi -- ne,
  ces ti -- mons man -- quent de thy -- mine…
  Pour la fai -- re, l'a -- ffaire, il su -- ffit
  d'un ban -- da -- ge, un sou -- da -- ge,
  u -- ne join -- tu -- re, u -- ne li -- ga -- tu -- re,
  un pan -- se -- ment, et un ra -- ccor -- de -- ment…
  Sou -- dons ça… Sou -- dons ça_! Sou -- dons ça_!
  Donc sou -- dons ça_! Sou -- dons ça…
  Sou -- dons ça là, sou -- dons ça…
  Sou -- dons ça_! Sou -- dons ça…

  Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_!
  Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_!
  Sou -- dons ça…
  Sou -- dons ça…
  Sou -- dons ça…
  Sou -- dons ça_!
}

ActeDeuxSceneDeuxSopranoDeuxTexte = \lyricmode {
  Vo -- ler… Vo -- ler…
  Vo -- ler…
  Vo -- ler, vo -- ler…
  Gli -- sser en l'air et les nu -- ées…
  S'é -- le -- ver, s'é -- le -- ver…
  Res -- ter en la clar -- té de l'A -- stre d'é -- té,
  et y brû -- ler de li -- ber -- té…
  Pour pa -- lli -- er ce -- tte cou -- rroie bri -- sée,
  mon  co -- lli -- er fe -- ra l'a -- ffaire…
  Un ban -- da -- ge_! Un sou -- da -- ge_!
  U -- ne join -- tu -- re_! Li -- ga -- tu -- re_!
  Pan -- se -- ment_! Et un ra -- ccor -- de -- ment…
  Sou -- dons ça… Sou -- dons ça_; sou -- dons ça_! Sou -- dons ça_!
  Sou -- dons ça sous ça_! Sou -- dons ça_; sou -- dons ça,
  sou -- dons ça, sou -- dons ça…
  Sou -- dons ça_! Sou -- dons ça…

  Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_!
  Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_! Sou -- dons ça_!
  Sou -- dons ça…
  Sou -- dons ça…
  Sou -- dons ça…
  Sou -- dons ça_!
}

ActeDeuxSceneDeuxContraltoTexte = \lyricmode {
  Brû -- lons ça… __ _
  Brû -- lons ça_! Brû -- lons ça…
  Brû -- lons ça,  brû -- lons ça.
  Brû -- lons ça,  brû -- lons ça_!
  Brû -- lons ça_; brû -- lons ça…
  Brû -- lons ça_; brû -- lons ça.
  Brû -- lons ça,  brû -- lons ça…
  Brû -- lons ça.
  Brû -- lons ça_; brû -- lons ça…
  Brû -- lons ça_! Brû -- lons ça…

  Brû -- lons ça_! Brû -- lons ça_! Brû -- lons ça_! Brû -- lons ça_!
  Brû -- lons ça_! Brû -- lons ça_! Brû -- lons ça_! Brû -- lons ça_!
  Brû -- lons ça…
  Brû -- lons ça…
  Brû -- lons ça…
  Brû -- lons ça_!
}

ActeDeuxSceneDeuxTenorTexte = \lyricmode {
  Vo -- yons ça… L'É -- _ tran -- ger est __ _ bi -- en ca -- lme_;
  vo -- yons ça… __ _
  Aah_!
  Aaah_! __ _
  Aaaah_! __ _
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- té_;
  Ma -- jes -- té, Ma -- jes -- té, Ma -- jes -- té_;
  Ma -- jes -- té, Ma -- jes -- té_— \leftSyl …_je
  viens vous sau -- ver_!
  Ma -- jes -- té_!
  Ma -- jes -- té, Ma -- jes -- té_!
  Je suis un scé -- lé -- rat_!
  C'est de ma fau -- te_; c'est de ma fau -- te tout ce -- la…
  Pu -- ni -- \dash ssez -- moi, pu -- ni -- \dash ssez -- moi…
  \leftSyl …_C'est u -- ne pu -- ni -- tion ro -- ya -- le -- ment
  é -- pou -- van -- ta -- ble, peu cha -- ri -- ta -- ble…
  A -- ccrou -- pi… mon ké -- pi va tom -- ber sans ré -- pit… __ _
  À vos ordres, à vos ordres_! Em -- por -- tons ça… Em -- por -- tons ça_!
  Por -- tons ça_! Por -- tons ça_! Por -- tons ça_! Por -- tons ça_!
  Por -- tons ça_! Por -- tons ça_! Por -- tons ça_! Por -- tons ça…
  Por -- tons ça…
  Por -- tons ça…
  Por -- tons ça…
  Por -- tons ça_!
}

ActeDeuxSceneDeuxBarytonUnTexte = \lyricmode {
  Ces briques… __ _ Je… Je me sou -- viens de ma mai -- son…
  Ces crânes… __ _ Je… Je me sou -- viens de mes a -- mis…
  Ces ba -- rreaux… Je me sou -- viens…
  La per -- go -- la sur la te -- rra -- sse.
  Le ro -- si -- er grim -- pant, les plan -- tes vi -- va -- ces…
  Ces o -- sse -- ments… Je me sou -- viens…
  \leftSyl …_ma mort est pour de -- main_!!
  C'é -- tait quoi, ça_? Sûre -- ment le Roi, je crois_;
  je re -- co -- nnais ce poids sur mes doigts…
  Vous a -- rri -- vez trop tard_; j'ai \dash moi -- mê -- me
  so -- nné ce ga -- illard.
  …_Il… \leftSyl …_Il é -- tait co -- staud_;
  très co -- staud_; ex -- trê -- me -- ment…
  \leftSyl …_co -- staud.
  Si vous vou -- lez…
  Me -- \dash ttez -- vous à qua -- tre pattes_!
  \leftSyl …_Et lé -- chez tous les cou -- loirs du pa -- lais.
  A -- LORS, em -- por -- \dash tez -- moi ce -- \dash lui -- là_!
  Di -- rec -- tion, __ _ la pri -- son_!
  VITE, plus vite, ex -- trê -- me -- ment plus vite_!
  Ca -- chons ça, ca -- chons ça…
  Ca -- chons ça_! Ca -- chons ça_! Ca -- chons ça_! Ca -- chons ça_!
  Ca -- chons ça_! Ca -- chons ça_! Ca -- chons ça…
}

ActeDeuxSceneDeuxBarytonDeuxTexte = \lyricmode {
  Chez moi, chez moi, je veux a -- ller chez moi_!
  Ma tête, ma tête, j'ai très mal à ma tête_;
  ex -- trê -- me -- ment mal à ma tête_!
  Ban -- dons ça… Ban -- dons ça…
  Ban -- dons ça… Ban -- dons ça…
  Ban -- dons ça,   ban -- dons ça…
  Ban -- dons ça_;  ban -- dons ça…
  Ban -- dons ça_!  Ban -- dons ça…
  J'ai mal, très mal_; ex -- trê -- me -- ment mal_!
  Do -- cteur, do -- cteur, des pi -- lules
  \leftSyl …_an -- ti_-
  \leftSyl …_dou -- leur…
  aaAAAh_! __ _ mh…
  Mmm… Mmm… Mmm… Mmm… Mmm… __ _ _ _ Mmm… __ _
}

%%%%%%%%%%%%%%%%%%%%  Troisième Tableau %%%%%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneTroisSopranoUnTexte = \lyricmode {
  Fi -- xons ça, fi -- xons ça…
  Vi -- ssons ça, vi -- ssons ça…
  Sci -- ons ça, sci -- ons ça…
  Po -- sons ça… \leftSyl …_po -- sons ça…
  La mé -- ca -- nique et l'être hu -- main \dash n'ont -- ils
  point maints points co -- mmuns_?
  Qu'on ou -- vre grand les portes_!
  Que la ma -- chi -- ne sorte_!
  Et que le vent l'em -- po -- rte_!
  À la forme de vos ban -- de -- lettes,
  je sais par -- faite -- ment qui vous êtes…
  Lai -- \dash ssez -- moi par -- tir a -- vec vous.
  E -- mme -- \dash nez -- moi, e -- mme -- \dash nez -- moi.
  En -- vo -- \dash lez -- moi, en -- vo -- \dash lez -- _ moi…
  I -- ci j'a -- sphy -- xie.
  E -- xac -- te -- ment, e -- xac -- te -- ment_!
  Et… les im -- pôts sont pour bien -- tôt_; je pré -- fè -- re
  m'en -- fuir a -- vec tout mon ma -- got.
  En -- vo -- \dash lez -- moi… __ _ En -- vo -- \dash lez -- moi_;
  e -- _ mme -- \dash nez -- moi, en -- vo -- \dash lez -- moi…
  E -- mme -- \dash nez -- moi…
  En -- vo -- \dash lez -- moi, e -- mme -- \dash nez -- moi…
  En -- vo -- \dash lez -- moi…
  E -- mme -- \dash nez -- moi…
  En -- vo -- \dash lez -- moi_!
  A -- RRÊ -- TEZ ÇA_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  É -- du -- ca -- tion_! In -- stru -- cti -- on_!
  Ci -- vi -- li -- sa -- tion_! Hu -- ma -- ni -- sa -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  tion tion tion tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  \dash —_tion_!
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
}

ActeDeuxSceneTroisSopranoDeuxTexte = \lyricmode {
  Mon a -- mi, quelle bo -- nne mine_;
  où donc a -- \dash llez -- vous, __ _ de ce pas __ _
  si ma -- gna -- nime_?
  L'É -- tran -- ger_? Ab -- sur -- di -- té_!
  Le peu -- ple se -- ra __ _ scan -- da -- li -- sé_!
  Non, non_; mais… \leftSyl …_Vous y a -- llez à pied_;
  des por -- teurs doi -- vent vous e -- mme -- ner…
  Ré -- pa -- rer la ma -- chi -- ne de __ _ l'É -- tran -- ger.
  Ne vous dé -- plai -- se.
  Fi -- xons ça_! Vi -- ssons ça_!
  \dash Scions -- \dash scions -- scions_!
  \dash Scions -- \dash scions -- scions_!
  A -- vec \dash ce -- lle -- ci, __ _ \dash peut -- _ on
  s'en -- vo -- ler, pla -- ner vi -- re -- vol -- ter,
  fen -- dre l'air et les nu -- ages de glace et de neige_?
  Qu'on ou -- vre grand les portes_!
  Que la ma -- chi -- ne sorte_!
  Et que le vent l'em -- po -- _ _ rte_!
  Je sais \leftSyl que_vous n'ê -- tes pas le Roi,
  mon ma -- ri a le nez plus droit…
  Lai -- \dash ssez -- moi par -- tir a -- vec vous.
  E -- mme -- \dash nez -- moi, e -- mme -- \dash nez -- moi.
  En -- vo -- \dash lez -- moi, en -- vo -- \dash lez -- moi…
  I -- ci, j'a -- sphy -- xie.
  Pas de ba -- gages_?
  Pas de ba -- gages_? Mais… __ _
  Mes ma -- lles et mes mer -- vei -- lleux bi -- joux_?
  Mes mille a -- tours et mes toi -- lettes_?
  Mes ro -- bes de fête, tou -- tes mes pa -- illettes_?
  E -- mme -- \dash nez -- moi…
  E -- mme -- \dash nez -- moi…
  E -- mme -- \dash nez -- moi_; en -- _ vo -- \dash lez -- moi,
  e -- mme -- \dash nez -- moi…
  E -- mme -- \dash nez -- moi…
  E -- mme -- \dash nez -- moi, en -- vo -- \dash lez -- moi_;
  E -- mme -- \dash nez -- moi, en -- vo -- \dash lez -- moi…
  En -- vo -- \dash lez -- moi_!
  A -- RRÊ -- TEZ ÇA_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  tion tion tion tion_! tion tion tion tion tion tion_!
  tion tion tion tion tion tion_;
  tion tion tion, tion tion tion tion tion_! tion tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  \dash —_tion_!
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _ _ _
}

ActeDeuxSceneTroisContraltoTexte = \lyricmode {
  Brû -- lons ça, __ _ brû -- lons ça…
  Brû -- lons ça, brû -- lons ça_;
  Brû -- lons ça, brû -- lons ça_!
  Brû -- lons ça, brû -- lons ça_!
  Brû -- lons ça, brû -- lons ça_!
  Brû -- lons ça… Brû -- lons ça…
  Dé -- trui -- sons ça_! __ _
  Brû -- lons ça, dé -- trui -- sons ça_!
  Ce -- tte ma -- chine est l'oeu -- vre du Mal_;
  elle doit fi -- nir en con -- fe -- ttis,
  brû -- lée pen -- due no -- yée_!
  Des -- truc -- tion_! Dé -- mo -- li -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  Ma -- lé -- di -- ction_!
  \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  \dash —_tion_!
  Aaa… __ _ _ _ _ _ _ _
}

ActeDeuxSceneTroisTenorTexte = \lyricmode {
  Bou -- \dash clez -- la, bou -- \dash clez -- la.
  Bou -- \dash clez -- la, bou -- \dash clez -- la.
  Bou -- \dash clez -- la, bou -- \dash clez -- la_;
  ça ne fait qu'un quart d'heure, un quart d'heure que vous ê -- tes là.
  Bou -- clez la_! __ _
  Fi -- xons ça_! Vi -- ssons ça_!
  \dash Scions -- \dash scions -- scions_!
  \dash Scions -- \dash scions -- scions_!
  Tout de sui -- te, ma -- jes -- té… __ _
  Qu'on ou -- vre grand les portes_!
  Que la ma -- chi -- ne sorte_!
  Et que le vent l'em -- po -- _ _ rte_!
  C'est lui, le Roi_!
  C'est lui, le Roi_!
  C'est lui, le Roi_! C'est lui_!
  A -- RRÊ -- TEZ ÇA_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  tion tion, tion tion tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  Aaa… __ _ _ _ _ _ _ _
  On ne les voit plus_; mais on les en -- tend hur -- ler beau -- coup trop.
  Plus per -- sonne à mettre en pri -- son…
  En É -- tran -- ger, vous é -- tiez fort_; mais là, en Roi, bra -- vo_!
  Cha -- peau bas_! Tous é -- li -- mi -- nés en u -- ne seu -- le fois…
  Vo -- tre so -- mmeil vous a tra -- hi.
  Ce n'est pas ce -- la_; mais ce ma -- tin, vous a -- vez re -- fait vo -- tre
  lit à mer -- veille.
  Vous a -- llez gou -- ver -- ner_! Or -- do -- nner_! Di -- ri -- ger_!
  Ce châ -- teau se -- ra vo -- tre nou -- veau vai -- sseau.
  Très i -- mmo -- bi -- le.
  Ex -- trê -- me -- ment i -- _ mmo -- bi -- le.
}

ActeDeuxSceneTroisBarytonUnTexte = \lyricmode {
  Que fai -- re_? Que fai -- re_? Que faire en cette a -- ffaire_?
  Je ne suis pas le Roi, je ne suis pas le Roi…
  Pru -- den -- ce, pru -- den -- ce_;
  c'est qu'il va me fa -- lloir jou -- er se -- rré, je le crois.
  À… À… À… \leftSyl …_À la pri -- son_; pour li -- bé -- rer
  l'É -- tran -- ger.
  Un Roi ne \dash peut -- il s'a -- bai -- sser
  à grâ -- ci -- er un tel pri -- so -- nnier_?
  Et vous, où a -- \dash llez -- vous_?
  La ré -- pa -- rer… J'ai -- me -- rais fort vous a -- ccom -- pa -- gner.
  \leftSyl …_À pied.
  Fi -- xons ça_! Vi -- ssons ça_!
  \dash Scions -- \dash scions -- scions_!
  \dash Scions -- \dash scions -- scions_!
  Do -- cteur, vous a -- vez fait mer -- veille…
  Voi -- là à quoi je ne m'a -- tten -- dais pas…
  Un geste, un mot me su -- ffi -- rait pour re -- tour -- ner chez moi_;
  que fai -- re_? Que  faire en cette a -- ffai -- re_?
  Ce -- tte ma -- chine a le don de tout com -- pli -- quer.
  M'en -- vo -- ler, m'en -- vo -- ler, et re -- trou -- ver la li -- ber -- té_?
  \dash Puis -- je fai -- re ce choix_? J'en ai le droit_: j'ai \ital tous les droits_;
  je __ _ suis __ _ le Roi, le __ _ Roi_! __ _
  Bon tra -- vail, très bon tra -- vail_; ex -- trê -- m…
  Oui_— ouu… __ _ ouille… Ou… __ _ Hou_— hou -- la -- la… euh…
  \leftSyl …_Et co -- mment le sau -- \dash rais -- je_? Hmm.
  A -- me -- \dash nez -- moi l'É -- tran -- ger.
  Et… __ _ Qu'on ou -- vre grand les portes_!
  Que la ma -- chi -- ne sorte_!
  Et que le vent l'em -- porte, et que le vent __ _ l'em -- po -- rte_!
  Moi_? Mais…
  Je com -- prends… Je com -- prends.
  Si vous n'a -- vez pas de ba -- gages,
  il \leftSyl y_au -- ra une pe -- ti -- te place.
  Vous_?
  Je com -- prends… Je com -- prends_:
  La po -- li -- ti -- que lo -- cale est i -- nhu -- mai -- ne_;
  vo -- tre mo -- ra -- le tou -- te mé -- di -- ca -- le
  ré -- prou -- ve la bar -- ba -- rie ro -- ya -- le…
  Mais… Mais…
  Un coup dans la trogne_?
  Vous qui a -- vez vou -- lu me tuer…
  \leftSyl …_Que di -- \dash riez -- vous d'être e -- xi -- lé_?
  C'est moi, le Roi_! C'est moi_!
  C'est moi, le Roi_: moi_!
  Le Roi c'est MOI_! Me -- \dash ttez -- le dans… la ma -- chine_!
  A -- RRÊ -- TEZ ÇA_!
  Et si je vous e -- mme -- nais plu -- tôt faire un tour,
  gli -- sser __ _ sur l'a -- zur, dé -- cou -- vrir un ho -- ri -- zon
  d'oi -- \dash seau -- lyre et __ _ de co -- ton_?
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  Me -- \dash ttez -- le donc dans l'a -- vion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  Aaa… __ _ _ _ _ _ _ _
  Ils se ba -- ttent pour a -- voir la pla -- ce près __ _ du hu -- blot.
  Ah… Vous m'a -- viez donc re -- co -- nnu, vous au -- ssi_?
  \dash Qu'ai -- je donc bien pu dire dans mon so -- mmeil_?
  Que \dash vais -- je bien pou -- voir faire, coin -- cé i -- ci_?
  Un voy -- age i -- mmo -- bi -- le.
  Ex -- trê -- me -- ment i -- mmo -- _ bi -- le.
}

ActeDeuxSceneTroisBarytonDeuxTexte = \lyricmode {
  Où \dash suis -- je_? Qu'est_-_ce donc que cet en -- droit_? __ _
  Pour -- quoi moi, de quel droit_? Je __ _ suis __ _ le Roi… __ _
  Sor -- \dash tez -- moi de là_! Sor -- \dash tez -- moi de là_!
  Je ne peux res -- ter là_! Je ne peux res -- ter là_;
  je suis le Roi, le __ _ Roi_! __ _
  Je ne peux res -- ter i -- \dash ci bas… __ _
  Je ne peux pas res -- ter_! Je ne peux pas res -- ter_;
  ces \dash gens -- là ne m'ont pas é -- té pré -- sen -- tés…
  Aah_! __ _ Ah_!
  Je suis là de -- puis des jours, des mois, des a -- nnées_;
  mon es -- to -- mac \dash doit -- il bra -- ver l'é -- ter -- ni -- té_?
  À man -- ger_! À man -- ger_! Du san -- gli -- er, du che -- vreuil,
  du man -- chot em -- pe -- reur_! __ _ Aah_! __ _ _ _ Aah_! __ _ _
  Ma cou -- ro -- nne, MA cou -- ro -- nne_!
  Aaah_! __ _ _ En -- fer -- \dash mez -- le_!
  C'est moi, le Roi_! C'est moi, moi_!
  C'est moi, le Roi_! C'est moi le Roi_!
  C'est moi  le Roi,  c'est moi le Roi_!
  C'est moi, le Roi,  c'est moi, le_—
  Eh bien… Tant mieux_!
  E -- mme -- \dash nez -- moi…
  E -- mme -- \dash nez -- moi_; en -- vo -- \dash lez -- moi… __ _
  En -- vo -- \dash lez -- moi… __ _
  I -- ci_; j'a -- sphy -- xie_!
  A -- RRÊ -- TEZ ÇA_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  Ex -- ploi -- ta -- tion_! Hé -- si -- ta -- tion_!
  Do -- mi -- na -- tion_! In -- _ dé -- ci -- sion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_!
  tion tion tion tion tion_! tion tion tion tion_; tion tion_!
  tion tion tion… tion tion tion_; tion tion_!
  \leftSyl \dash —_tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash tion -- \dash tion -- tion_!
  \dash tion -- \dash tion -- tion_! \dash A -- tten…
  \dash —_tion_!
  Aaa… __ _ _ _ _ _ _ _ _ _ _ _
}

