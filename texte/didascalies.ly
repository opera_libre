%------------------------------------------------------------------%
% Opéra Libre -- didascalies.ly                                    %
%                                                                  %
% (c) Lewis Trondheim & Valentin Villenave, 2008                   %
%                                                                  %
%------------------------------------------------------------------%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                  %
%                        AFFAIRE ETRANGERE                         %
%                                                                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Noir = \markup { \larger \rounded-box \smallCaps "Noir."}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Prologue                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PrologueAAA = "Ce tableau se joue à l'avant-scène, rideau fermé.
  La musique commence dans le noir."

PrologueA = "Entre le Chef de la Garde, par la même porte que le public.
Il traverse la salle vivement, semblant chercher quelqu'un dans le public,
jusqu'au moment où on le découvre dans la lumière."

PrologueB = "Il arrive enfin sur scène, face public, de plus en plus
exagérément lyrique..."

PrologueC = "Entre le Roi, déboulant sur scène par le centre du rideau,
derrière le Chef de la Garde, qui sursaute."

PrologueD = "Le Roi, peu intéressé, regarde dans le vague..."

PrologueE = "se fige..."

PrologueF = "... avant de réagir vivement."

PrologueG = "Le Chef se lamente avec affectation."

PrologueH = "Même jeu du Chef, derrière le Roi..."

PrologueI = "... qui se retourne brusquement pour le prendre à partie."

PrologueJ = "Un temps. Le Chef de la Garde hésite, va pour chanter,
puis se ravise. Même jeu. Puis enfin, avec un regard en coin vers le Roi :"

PrologueK = "Le Roi, après être resté pensif un moment, semble se ressaisir."

PrologueL = "Le Roi réfléchit, puis se penche un peu sur le côté."

PrologueM = "Le Roi pousse une exclamation de dégoût."

PrologueN = "Un temps. Le Roi et le Chef semblent chercher leurs mots..."

PrologueO = "mais ne parviennent qu'à crier derechef, ensemble."

PrologueP = "Le Roi, déconcerté et pensif, marmonne en cherchant ses mots"

PrologueQ = "Le Roi sort en continuant de chanter."

PrologueR = "Resté seul, le Chef regarde autour de lui, puis, au public :"



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              ACTE I                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%  Premier Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneUnAAA = "La lumière, montant avec la musique,
délimite bientôt une cellule étroite..."

ActeUnSceneUnA = "... dans
laquelle se trouve l’Étranger, allongé sur un
austère lit d’hôpital. Ses jambes sont
entravées par une lanière, et sa tête disparaît
sous des bandelettes."

ActeUnSceneUnB = "Peu à peu il s’éveille..."

ActeUnSceneUnC = "se redresse..."

ActeUnSceneUnD = "mais son mouvement lui arrache un cri de douleur."

ActeUnSceneUnE = "Il tourne la tête vers la droite : même jeu."

ActeUnSceneUnF = "Puis vers la gauche : même jeu, et ainsi de suite..."

%FIXME: variable names need some reordering.

ActeUnSceneUnFF = "Il s'active de plus en plus, produisant de ce fait
des cris variés et improbables."

ActeUnSceneUnFFF = "En se débattant, il en vient à s'assener un coup
dans l'entrejambe, et pousse un long cri plaintif et aigu."

ActeUnSceneUnG = "Épuisé, il finit par s’affaler sur le lit."

ActeUnSceneUnH = "Entrent le Roi et le Docteur, conversant avec agitation."

ActeUnSceneUnHH = "Interloqué, l'Étranger bafouille."

ActeUnSceneUnI = "Le Roi tente d’intervenir..."

ActeUnSceneUnJ = "mais se voit interrompre par le Docteur,
d’un geste péremptoire."

ActeUnSceneUnK = "Il sort de sa poche, l’un après l’autre, divers
objets — par exemple un canard en plastique,
un très long parapluie, un bilboquet cassé."

ActeUnSceneUnL = "Tour à tour, il les présente cérémonieusement
à l’Étranger, qui reste parfaitement impassible."

ActeUnSceneUnM = "Il lui présente enfin une vieille chaussette
toute rapiécée, devant laquelle l’Étranger semble réfléchir..."

ActeUnSceneUnN = "Le Docteur se tourne vers le Roi."

ActeUnSceneUnO = "Puis vers l’Étranger..."

ActeUnSceneUnP = "mais ce dernier semble perdu dans ses pensées ;"

ActeUnSceneUnQ = "et c’est le Roi qui finit par reprendre."

ActeUnSceneUnR = "Le Docteur va pour sortir, poursuivi par le
Roi qui, au comble de l’exaspération, finit par l’empoigner."

ActeUnSceneUnS = "Silence brusque. Le Roi se fige dans son
mouvement, sans même fermer la bouche."

ActeUnSceneUnT = "Puis on entend, dans le noir, la voix du Roi,
soudain très calme."

%------------------------------------------------------------------%

ActeUnSceneUnBisAAA = "La lumière revient peu à peu à mesure que la
musique monte. On découvre le Roi, figé dans
la même position à l’avant-scène. Le Docteur
a disparu."

ActeUnSceneUnBisA = "Le Roi s'anime tout d'un coup, dans la
même dynamique que précédemment."

ActeUnSceneUnBisB = "Il s’interrompt soudain, comme si une pensée
gênante lui venait. Il cherche à se ressaisir..."

ActeUnSceneUnBisC = "...mais s’interrompt à nouveau, de plus en
plus préoccupé."

ActeUnSceneUnBisD = "À cet instant seulement, il se souvient de la présence de
l’Étranger derrière lui et reste face public,
terrorisé à l’idée de se retourner."

ActeUnSceneUnBisE = "L’Étranger, qui pendant ce temps est parvenu
à défaire ses entraves, se lève."


%%%%%%%%%%%%%%%%%%%%%%  Deuxième Tableau  %%%%%%%%%%%%%%%%%%%%%%%%%%

InterludeUnA = "La lumière monte dans le silence. La scène
reste déserte un bref instant, puis Dieu entre
au moment où la musique commence. D’allure
quelconque, il est habillé en jardinier."

InterludeUnB = "Il parcourt la scène, inspectant le décor de papier, puis s’arrête devant les trois plantes à l’avant-scène."

InterludeUnC = "Il les inspecte une par une, les arrose..."

InterludeUnD = "Comme il écarte le feuillage, une feuille lui reste entre les doigts. Il réfléchit un instant, puis la mange."

InterludeUnE = "Arrivé à la troisième plante, il s'interrompt dans sa dégustation."

InterludeUnF = "Il regarde la plante, circonspect..."

InterludeUnG = "puis se met en devoir de la taillader, jusqu'à ce qu'il n'en reste plus rien qu'un tas épars."

InterludeUnH = "Du pied, il pousse les morceaux pour les dissimuler derrière les autres plantes."

InterludeUnI = "Il regarde autour de lui, puis avise un point hors de la scène et se met en chemin, tandis que la Reine entre par l’autre côté, perdue dans ses pensées, un coffret de bijoux entre les mains."

%------------------------------------------------------------------%

ActeUnSceneDeuxA = "Elle laisse sa phrase en suspens, absorbée dans sa rêverie."

ActeUnSceneDeuxAA = "Entre le Roi, sans la voir, de l'autre côté de la scène.
Elle le regarde s'avancer en silence, offrant un instant l'image d'une épouse
effacée, sinon craintive ; mais c'est sans le moindre effarouchement qu'elle
l'aborde."

ActeUnSceneDeuxB = "Le Roi reste figé un temps ; puis, manifestement embarrassé..."

ActeUnSceneDeuxC = "La Reine réfléchit."

ActeUnSceneDeuxCC = "Le Roi répond d'abord machinalement, puis il sursaute."

ActeUnSceneDeuxD = "Ravi, le Roi sort de sa poche un collier de pierres
précieuses, et va pour le lui donner en récompense..."

ActeUnSceneDeuxE = "mais il interrompt son geste, soudain très inquiet."

ActeUnSceneDeuxF = "Il s’interrompt, se rendant compte de l’idiotie
de sa répétition (il a toujours le collier à la
main). Après un temps, très séchement :"

ActeUnSceneDeuxG = "La Reine l'interrompt, prenant enfin et de
mauvaise grâce le collier des mains du Roi."

%%%%%%%%%%%%%%%%%%%%%%  Troisième Tableau %%%%%%%%%%%%%%%%%%%%%%%%%%

InterludeDeuxA = "Entre Dieu. Comme précédemment, il se dirige vers les plantes à l'avant-scène."

InterludeDeuxB = "Il regarde chacune des deux plantes, qui sont toutes deux bien plus grandes que lui. Il compare leurs tailles avec sa main, par rapport au haut de sa tête."

InterludeDeuxC = "Il décide subitement de les couper."

InterludeDeuxD = "Il les déchiquette de plus en plus vite... jusqu'à ce qu'elles ne forment plus qu'un tas sur le sol."

InterludeDeuxE = "Il regarde autour de lui mais ne trouve plus d'endroit pour cacher les débris."

InterludeDeuxF = "Il crée une grande flamme (ou de la fumée)..."

InterludeDeuxG = "... qui l'impressionne lui-même."

InterludeDeuxH = "Le feu éteint, il s'avance et regarde le théâtre."

%------------------------------------------------------------------%

ActeUnSceneTroisA = "La lumière revient sur la cellule de l'Étranger,
qui se tient immobile, seul. Entre le Chef de la
Garde, d'abord impérieux..."

ActeUnSceneTroisB = "puis soudain anodin, presque guilleret :"

ActeUnSceneTroisC = "Même jeu du Chef, très insouciant."

ActeUnSceneTroisD = "Silence. L'Étranger se fige face public, livide."

ActeUnSceneTroisE = "Le Chef s'approche peu à peu, bientôt gagné
par l'inquiétude de l'Étranger..."

ActeUnSceneTroisF = "Le Chef de la Garde se démène, agite les bras..."

ActeUnSceneTroisG = "et retrouve tout d'un coup sa désinvolture."

ActeUnSceneTroisH = "Le Chef, soudain pensif."

ActeUnSceneTroisI = "Devant la mine déconfite du Chef, l'Étranger
s'approche de lui à son tour, comme pour le consoler."

ActeUnSceneTroisJ = "La lumière s'éteint sur le Chef, laissant un
instant l'Étranger seul en scène, figé."

%------------------------------------------------------------------%

ActeUnSceneTroisBisA = "Quand la lumière revient, Dieu est de nouveau
en scène, à s'occuper des plantes comme pour un interlude."

ActeUnSceneTroisBisB = "Le Roi passe au fond sans le voir,
absorbé et marmonnant sombrement."

ActeUnSceneTroisBisC = "Un instant désarçonné, il   se   ressaisit."

ActeUnSceneTroisBisD = "Dieu  compulse son livre."

ActeUnSceneTroisBisE = "Il lui tend une bourse, dont Dieu se saisit
prestement."

ActeUnSceneTroisBisF = "Dieu s'éloigne doucement..."

ActeUnSceneTroisBisG = "Le Roi, resté seul en scène, contemple le plafond, hésitant...
Puis regarde dans la direction où Dieu vient de sortir."

%------------------------------------------------------------------%

ActeUnSceneTroisTerA = "Quand la lumière revient, l'Étranger est seul
en scène, silencieux."

ActeUnSceneTroisTerB = "Entre le Docteur, un dossier sous le bras."

ActeUnSceneTroisTerC = "Il l'entraîne un peu plus loin, vers un avion de
bois un peu disloqué."

ActeUnSceneTroisTerD = "Le Docteur ouvre son dossier et brandit une
tache du test de Rorschach."

ActeUnSceneTroisTerE = "Il lui montre une autre tache."

ActeUnSceneTroisTerF = "Le Docteur hésite un instant ; puis, montrant
le ciel :"

ActeUnSceneTroisTerG = "Il sort, laissant l'Étranger seul en scène."

%%%%%%%%%%%%%%%%%%%%%%  Quatrième Tableau %%%%%%%%%%%%%%%%%%%%%%%%%%

ActeUnSceneQuatreA = "La lumière monte dans le silence, dévoilant le
Roi, la Reine et l'Étranger attablés et en plein
dîner ; immobiles et muets, ils ne se regardent
pas."

ActeUnSceneQuatreB = "La musique s'interrompt en suspens, dans un
silence pesant. Le pianiste arpège un accord de dominante,
en une manière d'encouragement ; mais tour à tour, les personnages
vont pour chanter, et se ravisent."

ActeUnSceneQuatreC = "Même jeu, plusieurs fois ; enfin la
Reine prend la parole."

ActeUnSceneQuatreD = "L'Étranger reste silencieux."

ActeUnSceneQuatreE = "L'Étranger continue, pour lui-même..."

ActeUnSceneQuatreF = "Le Roi soupire sombrement."

ActeUnSceneQuatreG = "Regard rêveur et émerveillé de la Reine."

ActeUnSceneQuatreH = "Elle porte les mains à son collier, avec
un effroi soudain."

ActeUnSceneQuatreI = "L'Étranger fait mine de retrouver la mémoire..."

ActeUnSceneQuatreJ = "Il se verse à boire, et poursuit
sur un ton anodin."

ActeUnSceneQuatreK = "Il repose son verre dans un silence glacial."

ActeUnSceneQuatreL = "Entre Dieu."

ActeUnSceneQuatreM = "Dieu va pour sortir, mais le Roi le rattrape."

ActeUnSceneQuatreMM = "On entend le Chef accourir (soit de la coulisse,
soit de la salle)."

ActeUnSceneQuatreN = "Entre le Chef. On sent le Roi tenté d'embastiller
Dieu, mais un regard de ce dernier lui fait perdre
contenance ; il désigne alors l'Étranger."

ActeUnSceneQuatreO = "Le Chef entraîne l'Étranger dans la coulisse.
Dieu, au bout d'un instant, désoeuvré, finit par sortir
de l'autre côté."

ActeUnSceneQuatreP = "Seuls restent en scène le Roi et la Reine,
qui s'attablent pour finir leur dîner."

ActeUnSceneQuatreQ = "D'un geste désinvolte, elle désigne le menton
du Roi... au demeurant dissimulé sous une imposante barbe."

ActeUnSceneQuatreR = "Dans la salle (ou l'orchestre), on retrouve le
Chef de la Garde, qui emmène l'Étranger au cachot."

ActeUnSceneQuatreS = "Soudain l'Étranger s'immobilise."

ActeUnSceneQuatreT = "Le Chef recule aussitôt, et se protège
avec ses bras."

ActeUnSceneQuatreU = "L'Étranger tourne les talons et se met en chemin."

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             Entracte                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

EntracteA = "La musique commence dans le noir. Chaque
personnage est à un endroit différent de la
scène, seul et perdu dans ses pensées (divers
accessoires, bonnets de nuit ou autres,
suggèrent qu'ils n'arrivent pas à dormir). Les
lumières s'allument tour à tour pour faire
apparaître les différents personnages, à
mesure qu'ils prennent la parole (les autres
personnages restant alors dans le noir)."

EntracteB = "La Reine a toujours son collier a la main."

EntracteC = "Les personnages poursuivent chacun leur pensée..."

EntracteD = "Le Docteur ressort sa chausette de sa poche."

EntracteE = "Les personnages se figent.
Entre Dieu, chantonnant et cherchant des
plantes à entretenir. N'en trouvant pas, il se dirige
vers les personnages."

EntracteF = "Il commence à s'affairer autour du Chef.
Celui-ci s'anime et reprend le motif de Dieu, mais trop bas
(il peut prendre une posture scrofuleuse pour montrer
que quelque chose ne va pas)."

EntracteG = "Dieu lui redonne sa note, de façon
très pédagogue : mais le Chef, malgré ses efforts,
chante toujours trop grave."

EntracteH = "Dieu s'impatiente..."

EntracteI = "... Et le Chef de se lancer
soudain sur une nouvelle
note, trop aigüe cette fois.
Il la répète sans faiblir,
nobostant les signes que lui adresse Dieu
pour baisser sa note."

EntracteJ = "Dépité, Dieu se tourne maintenant
vers l'Étranger..."

EntracteK = "... qui part, à son tour, sur une note
trop grave. Avec une mine découragée, Dieu
abandonne la partie, laissant les autres chanteurs
entrer les uns après les autres."

EntracteL = "Sur un geste de Dieu, tous s'arrêtent.
Ils sont maintenant disposés en rang comme une chorale très appliquée,
que Dieu se met en devoir de diriger de façon un peu pataude."

EntracteM = "Ils restent figés en suspens ;
puis les lumières s'éteignent, laissant le Roi
seul, face au public."

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             ACTE II                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%  Premier Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneUnA = "Dans le noir et le silence, le Docteur
rassemble les débris de la machine volante (il
peut s'éclairer d'une lampe), en un espace
scénique à part – lequel peut se situer dans la
fosse, parmi le public, ou ailleurs. Alors qu'il
commence à les examiner, la musique
commence, et la lumière monte peu à peu. Le
rideau se lève sur un décor différent du
premier acte (il peut s'agir de la cour du
château)."

ActeDeuxSceneUnB = "Entre la Reine, qui traverse la scène, déserte,
pour rejoindre le Docteur ."

ActeDeuxSceneUnC = "Dubitatif, le Docteur fait les cent pas en
marmonnant."

ActeDeuxSceneUnD = "Entre Dieu, d'un pas assuré. Il se met à déambuler
sur scène tout comme le Docteur, mais sans
qu'ils ne se croisent ou se remarquent l'un
l'autre."

ActeDeuxSceneUnE = "Le Docteur se saisit d'un morceau de l'avion."

ActeDeuxSceneUnF = "Dieu brandit son livre."

ActeDeuxSceneUnG = "La Reine tente faiblement d'intervenir."

ActeDeuxSceneUnH = "La Reine intervient enfin avec autorité."

ActeDeuxSceneUnI = "Plus doucement, en regardant le ciel :"

ActeDeuxSceneUnJ = "La Reine reste songeuse."

%------------------------------------------------------------------%

ActeDeuxSceneUnBisA = "Quand la lumière revient, le Roi est en scène ;
il arbore un air réjoui et guilleret."

ActeDeuxSceneUnBisB = "Entre le Chef de la Garde, poussant l'Étranger à
coups de pied dans l'arrière-train."

ActeDeuxSceneUnBisC = "Un temps. L'Étranger reste sans voix, puis
reprend, perplexe..."
%FIXME
ActeDeuxSceneUnBisCC = "Il fait signe au Roi de s'approcher."

ActeDeuxSceneUnBisD = "Il lui assène un coup de poing en plein visage ;
le Roi s'effondre, évanoui, sous les yeux stupéfaits du Chef."

ActeDeuxSceneUnBisE = "Ce dernier reste sans voix, visiblement choqué."

ActeDeuxSceneUnBisF = "Tentant de se reprendre, il crie."

ActeDeuxSceneUnBisG = "Il s'arrête pour reprendre son souffle,
et regarde le Roi au sol, soudainement pensif, presque fasciné."

ActeDeuxSceneUnBisH = "Plus doucement, et pour lui-même."

ActeDeuxSceneUnBisI = "L'Étranger donne un coup de pied au Roi ; le
Chef se décompose."

ActeDeuxSceneUnBisJ = "Du pied, l'Étranger donne à nouveau un tout
petit coup au Roi."

ActeDeuxSceneUnBisK = "Le Chef s'approche et regarde ses pieds, visiblement tenté ;
mais des accords de l'orchestre l'interrompent.
Il hésite un peu, puis même jeu, toujours en regardant ses pieds. Enfin, au prix d'un effort manifeste,
il arme son pied..."

ActeDeuxSceneUnBisL = "Mais il se retrouve nez-à-nez avec le Roi, qui est revenu à lui et s'est relevé pendant son jeu de scène.
Le Chef recule précipitamment."

ActeDeuxSceneUnBisM = "Le Chef se prosterne."

ActeDeuxSceneUnBisN = "Le Chef interrompt l'Étranger,
presque en aboyant."

ActeDeuxSceneUnBisO = "L'Étranger s'interrompt, supris."

ActeDeuxSceneUnBisP = "L'Étranger lui redonne un coup de poing au
visage ; le Roi s'effondre de nouveau."

ActeDeuxSceneUnBisQ = "Le Chef gémit piteusement."

ActeDeuxSceneUnBisR = "Il va pour taper à nouveau le Roi, mais le
Chef le ceinture de ses bras pour l'en empêcher, et l'éloigne
tant bien que mal."

%%%%%%%%%%%%%%%%%%%%%%  Deuxième Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneDeuxA = "Dans le noir apparaît à nouveau la machine
de l'Étranger. La Reine est toujours à pied
d'oeuvre, en silence, comme si elle n'avait
cessé de s'y affairer pendant la scène
précédente."

ActeDeuxSceneDeuxB = "Soudain, sous l'effet d'une de ses
manipulations, la machine esquisse un
mouvement, ponctué par l'orchestre."

ActeDeuxSceneDeuxC = "La lumière diminue sur la Reine, sans
toutefois s'éteindre complètement. Sur scène,
on retrouve l'Étranger, au cachot ; on le voit
soit par une fenêtre ou un soupirail, soit par
un mur absent."

ActeDeuxSceneDeuxD = "Tout au long de la scène, chacun des
personnages se trouvera ainsi dans un espace
scénique différent, que la lumière délimitera à
chaque fois qu'il prendra la parole."

ActeDeuxSceneDeuxE = "Le Roi est sur un lit d'hôpital, maintenu par le
Docteur."

ActeDeuxSceneDeuxF = "Le Docteur sort une bande et entreprend de lui
panser la tête."

ActeDeuxSceneDeuxG = "Dieu est toujours occupé à tailler et arroser
les plantes."

ActeDeuxSceneDeuxH = "L'Étranger ramasse des ossements sur le sol
du cachot."

ActeDeuxSceneDeuxI = "Le Docteur semble dresser l'oreille, comme s'il
entendait la Reine au loin."

ActeDeuxSceneDeuxJ = "Distraitement, il sort, laissant le Roi avec son
bandage, interdit."

ActeDeuxSceneDeuxK = "De son côté, Dieu maugrée de plus en plus
vigoureusement."

ActeDeuxSceneDeuxL = "Le Docteur a rejoint la Reine."

ActeDeuxSceneDeuxM = "La Reine exécute, au fur et à mesure et de
plus en plus vite, les indications du Docteur."

ActeDeuxSceneDeuxN = "L'Étranger se saisit d'un tibia, très théâtralement."

ActeDeuxSceneDeuxO = "Entre le Chef de la Garde ; il se dirige vers
les oubliettes, dans l'ombre."

ActeDeuxSceneDeuxP = "La lumière éclaire soudain les oubliettes.
L'Étranger n'y est plus ; des os sont plantés
dans le mur et suggèrent une évasion."

ActeDeuxSceneDeuxQ = "Les lumières se rallument sur l'Étranger,
qui fuit à l'autre bout de la scène, son os à la main."

ActeDeuxSceneDeuxR = "Les lumières se rallument sur l'Étranger, un peu
plus loin ; et ainsi de suite."

ActeDeuxSceneDeuxS = "La lumière s'allume sur le Roi, dans son
lit. Sa couronne est posée sur sa tête désormais entièrement bandée."

ActeDeuxSceneDeuxT = "Mais c'est l'Étranger qui fait irruption dans la
lumière."

ActeDeuxSceneDeuxU = "Le Roi le reconnaît, et peine à finir sa phrase."

ActeDeuxSceneDeuxV = "Un temps. Ils se jaugent en silence, immobiles. Puis l'Étranger
lève lentement son bras, armé du tibia..."

ActeDeuxSceneDeuxW = "Le Roi, sentant venir le coup, lance un
long cri modulé..."

ActeDeuxSceneDeuxX = "... mais c'est de l'autre main que l'Étranger
l'assomme sèchement."

ActeDeuxSceneDeuxY = "L'Étranger desserre le poing et se dégourdit les doigts."

ActeDeuxSceneDeuxZ = "On entend le Chef accourir au loin. L'Étranger
regarde à gauche puis à droite..."

ActeDeuxSceneDeuxZA = "... enfin il ramasse la couronne et la met sur sa
tête, juste avant que ne surgisse le Chef."

%FIXME: needs reordering here.

ActeDeuxSceneDeuxZAA = "Ce dernier, qui s'apprêtait à faire une entrée
fracassante, opte au dernier moment pour une posture ostensiblement
désinvolte, faussement modeste."

ActeDeuxSceneDeuxZB = "Le Chef de la Garde, suspicieux, regarde le Roi
au sol, puis l'Étranger, qui escamote précipitamment le tibia dans son dos."

ActeDeuxSceneDeuxZBB = "Le Chef se jette à genoux."

ActeDeuxSceneDeuxZC = "Le Chef ne bouge pas."

ActeDeuxSceneDeuxZD = "Pendant que le Chef s'éloigne, l'Étranger se met
en quête d'une cachette pour son os."

ActeDeuxSceneDeuxZDD = "Dans le noir, on entend peu à peu tous les
personnages, chacun plongés dans leur tâche..."

%%%%%%%%%%%%%%%%%%%%%%  Troisième Tableau   %%%%%%%%%%%%%%%%%%%%%%%%%

ActeDeuxSceneTroisA = "La scène s'éclaire en totalité, révélant une situation
nouvelle : le Roi est dans les oubliettes, quelque peu hagard. Le Chef de la Garde se
tient à la porte, de pied ferme."

ActeDeuxSceneTroisB = "Entre l'Étranger, discrètement de l'autre côté."

ActeDeuxSceneTroisC = "Le Roi crie à la cantonnade, avec véhémence."

ActeDeuxSceneTroisD = "Il désigne un des squelettes dans le cachot."

ActeDeuxSceneTroisE = "Le Chef de la Garde tance le Roi sans se retourner."

ActeDeuxSceneTroisF = "Alors que l'Étranger tente de traverser la
scène en catimini, entre la Reine, chargée
d'outils divers et plus ou moins incongrus."

ActeDeuxSceneTroisG = "Comme s'il l'avait entendu, le Roi
signale bruyamment sa présence, pendant que le Chef demeure imperturbable."

ActeDeuxSceneTroisH = "Elle s'approche de lui et chuchote afin que le Chef
n'entende pas."

ActeDeuxSceneTroisI = "Le Chef, qui tendait l'oreille,
rectifie la position, comme piqué au vif."

ActeDeuxSceneTroisJ = "Dieu s'affaire toujours sur des plantes, de plus en plus
virulent."

ActeDeuxSceneTroisK = "L'Étranger désigne les outils de la Reine."

ActeDeuxSceneTroisL = "À ces mots, le Chef de la Garde accourt et se baisse
comme pour le porter."

ActeDeuxSceneTroisM = "Pendant qu'ils montent tous trois vers le fond de la
scène (où l'on aperçoit les portes du château,
donnant sur le ciel), Dieu passe en trombe à
l'avant scène, arrachant et malmenant les
plantes dans une frénésie destructrice."

ActeDeuxSceneTroisN = "La machine volante, pleinement reconstituée,
est amenée sur scène, devant les personnages réunis autour du Docteur, qui dirige la
manoeuvre. Elle est manifestement en marche (ce que peut suggérer une hélice en
mouvement, ou tout autre moyen), et prête à décoller . L'orchestre reprend le
motif correspondant au mouvement du moteur de la machine."

ActeDeuxSceneTroisO = "Le Docteur, cependant que la machine
s'immobilise, s'empare des outils que lui a apportés la Reine,
pour mettre la dernière main aux réparations."

ActeDeuxSceneTroisP = "Le Docteur sort une énorme scie pour
éliminer l'extrémité d'un tout petit tasseau qui dépasse."

ActeDeuxSceneTroisQ = "Son travail terminé, le Docteur se fige,
visiblement mal à l'aise avec ses outils. Lentement, il se baisse
pour les poser au sol."

ActeDeuxSceneTroisR = "Ému, l'Étranger descend vers l'avant-scène en
examinant la machine. Les autres personnages s'immobilisent pendant son
aparté."

ActeDeuxSceneTroisS = "Il s'enflamme peu à peu."

ActeDeuxSceneTroisT = "Un temps. Il retrouve lentement ses esprits,  et
remonte rejoindre les autres personnages."

ActeDeuxSceneTroisU = "Il reprend sa conversation avec le Docteur,
machinalement."

ActeDeuxSceneTroisV = "Il s'interrompt, surpris de son propre tic de
langage ; le Docteur poursuit sans lui prêter la moindre attention."

ActeDeuxSceneTroisW = "L'Étranger s'apprête à dire «oui», mais se retient
de justesse."

ActeDeuxSceneTroisX = "Le Chef de la Garde s'incline ;
au moment où il va pour sortir, l'Étranger le retient."

ActeDeuxSceneTroisY = "Le Chef sort.
La Reine entraîne l'Étranger sur le côté pour lui
parler à part."

ActeDeuxSceneTroisZ = "Pendant que la Reine réfléchit, perturbée, le
Docteur prend à son tour l'Étranger par le bras et l'entretient en aparté."

ActeDeuxSceneTroisZA = "Le Docteur et la Reine tirent l'Étranger par les
bras, chacun dans une direction différente."

ActeDeuxSceneTroisZB = "Entre le Roi, déboulant en trombe les bras tendus,
et retenu tant bien que mal par le Chef de la Garde."

ActeDeuxSceneTroisZC = "L'Étranger arme son bras ; le Roi recule vivement."

ActeDeuxSceneTroisZCC = "L'Étranger s'interrompt en plein effet dramatique,
réfléchit un instant, regarde la machine, puis reprend sur un ton plus léger."

ActeDeuxSceneTroisZD = "Le Roi se tourne vers le Chef, plus implorant
qu'impérieux..."
%FIXME
ActeDeuxSceneTroisZDD = "L'Étranger désigne la machine d'un geste théâtral,
et hésite un instant pour la dénommer."

ActeDeuxSceneTroisZE = "Le Chef donne un petit coup de poing sur la
tête du Roi..."

ActeDeuxSceneTroisZF = "... qui se résoud à obtempérer. Le Chef de la Garde
regarde son poing avec admiration."

ActeDeuxSceneTroisZG = "Le chef de la Garde pousse le Roi vers l'avion ;
mais Dieu, encombré de ses sécateurs, s'interpose théâtralement."

ActeDeuxSceneTroisZH = "Sous les regards interloqués de l'assistance, le
Docteur bouscule Dieu (sans un regard), et entreprend de grimper dans la machine."

ActeDeuxSceneTroisZI = "Le Roi s'avance et embarque laborieusement."

ActeDeuxSceneTroisZJ = "Tout le monde se tourne vers la Reine,
qui semble chercher en vain des rimes en
«tion»... Le choeur répète son motif,
pour l'encourager."

ActeDeuxSceneTroisZK = "Elle finit par grimper à son tour dans
la machine, chantant sans paroles mais avec beaucoup de conviction."

ActeDeuxSceneTroisZL = "Tous les regards se tournent maintenant
vers l'Étranger ; mais c'est Dieu qu'il désigne."

ActeDeuxSceneTroisZM = "Le Chef de la Garde s'exécute, enhardi
par le coup de poing qu'il a donné au Roi."

ActeDeuxSceneTroisZN = "En prenant place, Dieu bouscule le Roi, qui choit
sur une grosse manette. La machine a un soubresaut, et la musique s'interrompt."

ActeDeuxSceneTroisZO = "Soudain propulsée, la machine volante disparaît
rapidement, sous le regard dépité de l'Étranger."

ActeDeuxSceneTroisZP = "Au fond, le Chef regarde au loin et semble
suivre des yeux la chute de l'avion."

ActeDeuxSceneTroisZQ = "En silence, le Chef vient rejoindre l'Étranger,
face au public, à l'avant-scène. Après un temps, il lui adresse
quelques signes de connivence, avec un sourire sans malice."

ActeDeuxSceneTroisZR = "Un temps. L'Étranger soupire."

ActeDeuxSceneTroisZS = "Le Chef désigne la salle, le public, le théâtre."
